var express = require("express");
var router = express.Router();

const { MobileUser } = require("../src/model/mobileUser");

/* GET users listing. */
router.post("/login/", async (req, res, next) => {
  try {
    let mUser = new MobileUser();
    mUser.mobileNumber = req.body.mobileNumber;
    let pword = null;
    if (req.body.password !== undefined) {
      pword = req.body.password;
    }
    let response = await mUser.newUserAction(pword);
    console.log(":>", response);
    res.send(response);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
