var express = require("express");
var router = express.Router();

const VerifyCode = require("../src/model/verifycode");
const { MobileUser } = require("../src/model/mobileUser");
const MAuthentication = require("../src/model/mAuthentication");

router.post("/userVerification/", async (req, res, next) => {
  try {
    var verifyCode = new VerifyCode();
    verifyCode.userId = req.body.userId;
    verifyCode.code = req.body.code;
    let password = req.body.password;
    let msg = await verifyCode.verify();
    let feedBack = false;
    let pwordStatus = false;
    if (msg) {
      let mUser = new MobileUser();
      mUser.id = req.body.userId;
      feedBack = await mUser.edit();
      if (feedBack) {
        let mAuth = new MAuthentication();
        mAuth.userId = req.body.userId;
        mAuth.pword = password;
        pwordStatus = await mAuth.add();
      }
    }
    let returnVal = null;
    if (pwordStatus === true && feedBack === true && msg === true) {
      returnVal = {
        status: "Verified",
      };
    } else {
      returnVal = {
        status: "Verify",
      };
    }
    console.log(">:return message ", returnVal);
    res.send(returnVal);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
