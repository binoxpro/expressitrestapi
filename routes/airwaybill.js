var express = require("express");
var router = express.Router();

const Airwaybill = require("../src/model/airwaybill");
const Signature = require("../src/model/signature");
const TrackingPassCode = require("../src/model/trackingpasscode");
const ShipmentPosition = require("../src/model/shipmentposition");

router.post("/list/", async (req, res, next) => {
  try {
    let mAirwayBill = new Airwaybill();
    let userId = req.body.userId;
    let status = req.body.status;
    //console.log("Status is ", status);
    let response = await mAirwayBill.viewall(userId, status);
    res.send(response);
  } catch (err) {
    next(err);
  }
});
router.post("/starttrip/", async (req, res, next) => {
  try {
    console.log(">:Driver is Requesting to start trip");
    let airwaybill = new Airwaybill();
    let airwaybillId = req.body.airwaybillId;
    let userId = req.body.userId;

    let result = await airwaybill.findOrder(airwaybillId, userId);
    if (result) {
      res.send("Trip successfully started");
    } else {
      res.send("Some thing went wrong please contact Xonib support");
    }
  } catch (err) {
    next(err);
  }
});
router.post("/positionDriver/", async (req, res, next) => {
  try {
    let airwaybill = new Airwaybill();
    let driverId = req.body.driverId;
    let lat = req.body.lat;
    let lng = req.body.lng;
    airwaybill
      .locateDriver(driverId)
      .then(async (rs) => {
        console.log(":>Run front ", rs);
        console.log("");
        let shipmentPosition = new ShipmentPosition();
        let x = await rs.forEach(async (row) => {
          shipmentPosition.airwaybillId = row["airwaybillId"];
          shipmentPosition.lat = lat;
          shipmentPosition.lng = lng;
          shipmentPosition.creationDate = airwaybill.today();
          shipmentPosition.creationtime = airwaybill.currentTime();
          shipmentPosition.isActive = 1;
          let c = await shipmentPosition.getCurrent();
          //console.log(":>Status ", status);
        });
      })
      .then(() => {
        res.send("Positing done");
      });
    //console.log(":>", result);
    // if (result.length > 0) {
    //   res.send("Positing done");
    // } else {
    //   res.send("No Position");
    // }
  } catch (err) {
    next(err);
  }
});
router.post("/completeReceipt/", async (req, res, next) => {
  try {
    console.log(">:Start Driver Complete Trip");
    let airwaybill = new Airwaybill();
    let signature = new Signature();
    let trackingpasscode = new TrackingPassCode();

    let airwaybillId = req.body.airwaybillId;
    let userId = req.body.userId;
    let noOfPieces = req.body.noOfPieces;
    let volWeight = req.body.volWeight;
    let actualWeight = req.body.actualWeight;
    let formName = req.body.formName;
    signature.textSignature = req.body.textSignature;
    signature.airwaybillId = airwaybillId;
    trackingpasscode.airwaybillId = airwaybillId;
    console.log(">:Parameter's Received");
    console.log(
      airwaybillId,
      userId,
      noOfPieces,
      volWeight,
      actualWeight,
      signature.textSignature
    );

    //res.send("ok");
    let res1 = await airwaybill.updateDetails(
      airwaybillId,
      noOfPieces,
      volWeight,
      actualWeight,
      userId
    );
    console.log("Status of Update: ", res1);
    if (res1) {
      let res2 = await signature.save();
      if (res2) {
        console.log("Form Name", formName);
        if (formName === "Confirm Receipt") {
          let res3 = await trackingpasscode.save();
          if (res3) {
            let res4 = await trackingpasscode.sendSmsCode();
          }
        } else {
          let res5 = await trackingpasscode.sendSmsCode2();
        }
      }
      res.send("Saved");
    } else {
      res.send("Not Saved");
    }
  } catch (err) {
    next(err);
  }
});

module.exports = router;
