const env = process.env;
const config = {
  db: {
    host: env.DB_HOST || "mysql-61469-0.cloudclusters.net",
    port: env.DB_PORT || "10868",
    user: env.DB_USER || "admin",
    password: env.DB_PASSWORD || "xSCZEr2X",
    database: env.DB_NAME || "expressit_db",
    waitForConnection: true,
    connectionLimit: env.DB_CONN_LIMIT || 25,
    queueLimit: 0,
    debug: env.DB_DEBUG || false,
  },
};

module.exports = config;
