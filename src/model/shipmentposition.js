const db = require("../utils/db");

const ShipmentPosition = class {
  constructor() {
    this.id = null;
    this.airwaybillId = null;
    this.lat = null;
    this.lng = null;
    this.place = null;
    this.district = null;
    this.creationDate = null;
    this.creationtime = null;
    this.isActive = null;
  }

  save = async () => {
    console.log(">:The Driver Location is begin to get saved");
    try {
      let sql =
        "INSERT INTO `shipmentposition`( `airwaybillId`, `lat`, `lng`, `creationDate`, `creationTime`, `isActive`) VALUES(?,?,?,?,?,?)";
      let result = await db.query(sql, [
        this.airwaybillId,
        this.lat,
        this.lng,
        this.creationDate,
        this.creationtime,
        this.isActive,
      ]);
      if (result.affectedRows) {
        console.log(">:Driver location Saved successfull");
        return true;
      } else {
        console.log(">:Driver location was not saved successfully");
        return false;
      }
    } catch (error) {
      console.error(">:", error);
      return false;
    }
  };

  getCurrent = async () => {
    try {
      console.log(":>Getting the current position");
      let sql =
        "Select * from shipmentposition where airwaybillId=? and isActive=1";
      let result = await db.query(sql, [this.airwaybillId]);
      if (result.length > 0) {
        console.log(":>Found a current position value");
        result.forEach(async (row) => {
          console.log(":>Updating the Current position ");
          let sql2 = "Update shipmentposition set isActive=0 where id =?";
          let result2 = await db.query(sql2, [row["id"]]);
        });
        let r2 = await this.save();
        return r2;
      } else {
        console.log(":>Found No Current position ");
        console.log(":>Updating the Current position ");
        let r = await this.save();
        return r;
      }
    } catch (error) {
      console.error(":>", error);
      return false;
    }
  };
};

module.exports = ShipmentPosition;
