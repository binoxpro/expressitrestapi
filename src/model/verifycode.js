const db = require("../utils/db");
const axios = require("axios");
const { MobileUser } = require("./mobileUser");
const VerifyCode = class {
  constructor() {
    this.id = -1;
    this.userId = null;
    this.code = null;
    this.isActive = null;
    this.creationDate = null;
    this.verificationDate = null;
  }

  /*smsObject = {
    method: "SendSms",
    userdata: {
      username: "james2yiga@gmail.com",
      password: "AWh79JxpD72CmXJ",
    },
    msgdata: [
      {
        number: number,
        message: message,
        senderid: "quiktranzi",
      },
    ],
  };*/

  generationCode = () => {
    let letter = [
      "a",
      "A",
      "B",
      "C",
      "D",
      "E",
      "F",
      "G",
      "H",
      "J",
      "I",
      "L",
      "M",
      "N",
      "O",
      "P",
      "Q",
      "R",
      "S",
      "T",
      "U",
      "V",
      "W",
      "X",
      "Y",
      "Z",
      "0",
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
    ];
    var sizeof = letter.length;
    let str = null;
    for (var i = 0; i < 5; i++) {
      if (i === 0) {
        str = letter[this.getRandomInt(0, sizeof)];
      } else {
        str += letter[this.getRandomInt(0, sizeof)];
      }
    }
    return str;
  };

  add = async () => {
    let message = false;
    if (!(await this.checkActiveCode())) {
      console.log("Save the User Code");
      const result = await db.query(
        "INSERT INTO `verifycode`(`userId`, `code`, `isActive`) VALUES (?,?,1) ",
        [this.userId, this.generationCode()]
      );

      if (result.affectedRows) {
        message = true;
      }
      return message;
    } else {
      //update the code
      console.log("Update the User Code");
      //message = await this.editCode();
      return await this.editCode();
    }
  };

  checkActiveCode = async () => {
    let status = false;
    const result = await db.query(
      "Select * from verifycode where userId =? and isActive=1",
      [this.userId]
    );
    if (result.length > 0) {
      status = true;
    }
    return status;
  };

  verify = async () => {
    let status = false;
    console.log("Verification Task:", this.userId, this.code);
    const result = await db.query(
      "Select * from verifycode where userId =? and code = ? and isActive=1",
      [this.userId, this.code]
    );
    if (result.length > 0) {
      result.forEach((row) => {
        this.id = row["id"];
      });
      this.isActive = 0;
      let message = await this.edit();

      console.log("User update done:", message);

      status = true;
    }

    return status;
  };

  getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
  };

  edit = async () => {
    const result = await db.query(
      "Update `verifycode` set isActive=? where id=? ",
      [0, this.id]
    );
    let message = "Error in editing quote";
    if (result.affectedRows) {
      message = "Verification Code Created successfully";
    }

    return { message };
  };
  editCode = async () => {
    console.log("Inside the update function");
    let message = false;
    const result = await db.query(
      "Update `verifycode` set code=? where userId=? and isActive=1 ",
      [this.generationCode(), this.userId]
    );

    console.log("Size", result.affectedRows);
    if (result.affectedRows) {
      console.log("Inside the update function");
      message = true;
    }

    return message;
  };

  sendSmsCode = async (user) => {
    let response;
    let message = "Starting";
    let number = "+256781587081";
    try {
      const data = await db.query(
        "Select concat('+256',substr(u.mobileNumber,2,9) ) as number,Concat('Use verification code ', vc.code,' for Express It Logistic app authentication.') as message from verifycode vc inner join mobileuser u on vc.userId=u.id where vc.userId=? ",
        [user.id]
      );
      console.log(data);
      data.forEach((row) => {
        number = row["number"];
        message = row["message"];
      });

      let smsObject = {
        method: "SendSms",
        userdata: {
          username: "james2yiga@gmail.com",
          password: "AWh79JxpD72CmXJ",
        },
        msgdata: [
          {
            number: number,
            message: message,
            senderid: "expressit",
          },
        ],
      };
      response = await axios.post(
        "http://www.egosms.co/api/v1/json/",
        smsObject
      );

      //console.log("Feedback from EgoSms Server, ", response.data);
      return "Sent";
    } catch (err) {
      return err.messag;
    }
  };
  getVerificationStatus = async (userId) => {
    let status = false;
    const result = await db.query(
      "Select case when st.statusName='verify' then 0 else 1 end userStatus from user u inner join statu st on u.statusId = st.id where u.userId=?",
      [userId]
    );
    if (result.length > 0) {
      result.forEach((row) => {
        status = row["userStatus"] === 1 ? true : false;
      });
    }

    return status;
  };
};

module.exports = VerifyCode;
