const db = require("../utils/db");
const axios = require("axios");
const { encrypt, decrypt } = require("../utils/stringcrypto");

const MAuthentication = class {
  constructor() {
    (this.id = -1),
      (this.userId = null),
      (this.pword = null),
      (this.isActive = null),
      (this.creation = null),
      (this.modificationDate = null);
  }

  count = async () => {
    let sql = "Select * from mauthentication where userId = ?";
    const result = await db.query(sql, [this.id]);
    return result.length;
  };
  add = async () => {
    let status = false;
    let state = null;
    let sql = null;
    let credentialCount = 0;
    credentialCount = await this.count();
    this.pword = encrypt(this.pword);
    if (credentialCount === 0) {
      sql = "Insert into mauthentication (userId,pword,isActive) value(?,?,1)";
      state = "Insert";
    } else {
      //update the result
      state = "update";
      sql = "Update mauthentication set pword =? where userId=?";
    }
    const result = await db.query(
      sql,
      state === "Insert" ? [this.userId, this.pword] : [this.pword, this.userId]
    );
    if (result.affectedRows) {
      status = true;
    }
    return status;
  };
  login = async () => {
    let sql = "Select * from mauthentication where userId = ?";
    const result = await db.query(sql, [this.userId]);
    console.log("Pword= ", this.pword, " Length:", result.length);
    var matchFound = 0;
    if (result.length > 0) {
      console.log(result);
      var hash = null;
      result.forEach((row) => {
        hash = this.getHash(row["pword"]);
      });
      var pwordDB = decrypt(hash);
      console.log("Db Password:", pwordDB, " pword:", this.pword);
      if (this.pword === pwordDB) {
        matchFound = 1;
      }
    }
    return matchFound === 1 ? true : false;
  };
  getPword = () => {
    this.pword = encrypt(this.pword);
  };
  getHash = (row) => {
    let hashs = row.split("-");
    return {
      iv: hashs[0],
      content: hashs[1],
    };
  };
};

module.exports = MAuthentication;
