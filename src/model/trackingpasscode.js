const db = require("../utils/db");

const axios = require("axios");

const TrackingPassCode = class {
  constructor() {
    this.id = null;
    this.airwaybillId = null;
    this.passcode = null;
    this.creationDate = null;
    this.isActive = null;
  }

  generationCode = () => {
    let letter = [
      "a",
      "A",
      "B",
      "C",
      "D",
      "E",
      "F",
      "G",
      "H",
      "J",
      "I",
      "L",
      "M",
      "N",
      "O",
      "P",
      "Q",
      "R",
      "S",
      "T",
      "U",
      "V",
      "W",
      "X",
      "Y",
      "Z",
      "0",
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
    ];
    var sizeof = letter.length;
    let str = null;
    for (var i = 0; i < 5; i++) {
      if (i === 0) {
        str = letter[this.getRandomInt(0, sizeof)];
      } else {
        str += letter[this.getRandomInt(0, sizeof)];
      }
    }
    return str;
  };

  getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
  };

  save = async () => {
    try {
      console.log(":>Creating the passcode");
      this.creationDate = this.today();
      this.passcode = this.generationCode();
      let sql =
        "INSERT INTO trackingpasscode (airwaybillId,passcode,creationDate,isActive) VALUES(?,?,?,?)";
      let result = await db.query(sql, [
        this.airwaybillId,
        this.passcode,
        this.creationDate,
        1,
      ]);
      if (result.affectedRows) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      console.log(":>Error while creating the Passcode :: '" + error + "'");
      return false;
    }
  };

  currentTime = () => {
    let date_ob = new Date();
    // current hours
    let hours = date_ob.getHours();
    // current minutes
    let minutes = date_ob.getMinutes();
    // current seconds
    let seconds = date_ob.getSeconds();
    return hours + ":" + minutes + ":" + seconds;
  };

  today = () => {
    let date_ob = new Date();
    let date = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + date_ob.getMonth() + 1).slice(-2);
    let year = date_ob.getFullYear();
    return year + "-" + month + "-" + date;
  };
  sendSmsCode2 = async () => {
    let response;
    let message = "Starting";
    let number = null;
    let numberR = null;
    try {
      const data = await db.query(
        "Select concat('+256',substr(va.phoneNumber,2,9) ) as phoneNumber,concat('+256',substr(va.phoneNumberR,2,9) ) as phoneNumberR,tp.passcode from vwairwaybill va inner join trackingpasscode tp on va.id=tp.airwaybillId where va.id=? ",
        [this.airwaybillId]
      );
      console.log(data);

      data.forEach((row) => {
        number = row["phoneNumber"];
        numberR = row["phoneNumberR"];
        message =
          "Your shippment has been delivered to Destination Thank you for choosing Express it logistics ltd";
      });

      let smsObject = {
        method: "SendSms",
        userdata: {
          username: "james2yiga@gmail.com",
          password: "AWh79JxpD72CmXJ",
        },
        msgdata: [
          {
            number: number,
            message: message,
            senderid: "expressit",
          },
        ],
      };

      /*
      ,
          {
            number: numberR,
            message: message,
            senderid: "expressit",
          },
      
      */

      let smsObject2 = {
        method: "SendSms",
        userdata: {
          username: "james2yiga@gmail.com",
          password: "AWh79JxpD72CmXJ",
        },
        msgdata: [
          {
            number: numberR,
            message: message,
            senderid: "expressit",
          },
        ],
      };
      response = await axios.post(
        "http://www.egosms.co/api/v1/json/",
        smsObject
      );
      let response2 = await axios.post(
        "http://www.egosms.co/api/v1/json/",
        smsObject2
      );

      //console.log("Feedback from EgoSms Server, ", response.data);
      return "Sent";
    } catch (err) {
      return err.messag;
    }
  };
  sendSmsCode = async () => {
    let response;
    let message = "Starting";
    let number = null;
    let numberR = null;
    try {
      const data = await db.query(
        "Select concat('+256',substr(va.phoneNumber,2,9) ) as phoneNumber,concat('+256',substr(va.phoneNumberR,2,9) ) as phoneNumberR,tp.passcode from vwairwaybill va inner join trackingpasscode tp on va.id=tp.airwaybillId where va.id=? ",
        [this.airwaybillId]
      );
      console.log(data);

      data.forEach((row) => {
        number = row["phoneNumber"];
        numberR = row["phoneNumberR"];
        message =
          "Your shippment tracking refCode is " +
          this.airwaybillId +
          "-" +
          row["passcode"] +
          "Track it from https://expressitlogisticstrackit.com";
      });

      let smsObject = {
        method: "SendSms",
        userdata: {
          username: "james2yiga@gmail.com",
          password: "AWh79JxpD72CmXJ",
        },
        msgdata: [
          {
            number: number,
            message: message,
            senderid: "expressit",
          },
        ],
      };

      /*
      ,
          {
            number: numberR,
            message: message,
            senderid: "expressit",
          },
      
      */

      let smsObject2 = {
        method: "SendSms",
        userdata: {
          username: "james2yiga@gmail.com",
          password: "AWh79JxpD72CmXJ",
        },
        msgdata: [
          {
            number: numberR,
            message: message,
            senderid: "expressit",
          },
        ],
      };
      response = await axios.post(
        "http://www.egosms.co/api/v1/json/",
        smsObject
      );
      let response2 = await axios.post(
        "http://www.egosms.co/api/v1/json/",
        smsObject2
      );

      //console.log("Feedback from EgoSms Server, ", response.data);
      return "Sent";
    } catch (err) {
      return err.messag;
    }
  };
};

module.exports = TrackingPassCode;
