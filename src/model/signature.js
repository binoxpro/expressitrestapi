const db = require("../utils/db");

const Signature = class {
  constructor() {
    this.id = null;
    this.textSignature = null;
    this.airwaybillId = null;
    this.creationDate = null;
  }

  viewall = async () => {
    try {
      let sql = "Select * from signature order by id";
      const result = await db.query(sql);
      return result;
    } catch (err) {
      return err.message;
    }
  };
  viewForAnyWayBill = async (airwaywaybillId) => {
    try {
      let sql = "Select * from signature where airwaybillId =? order by id";
      const result = await db.query(sql, [airwaywaybillId]);
      return result;
    } catch (error) {}
  };

  save = async () => {
    try {
      console.log(
        ":>Saving the Signature : The Parameters to save",
        this.textSignature,
        this.airwaybillId
      );
      let sql =
        "INSERT INTO signature (textSignature,airwaybillId) VALUES(?,?)";
      const result = await db.query(sql, [
        this.textSignature,
        this.airwaybillId,
      ]);
      if (result.affectedRows) {
        console.log(":>Signature Saved");
        return true;
      } else {
        console.log(":>No Signature Saved");
        return false;
      }
    } catch (error) {
      console.log("Error:> While Saving signature ", error.message);
      return false;
    }
  };
};

module.exports = Signature;
