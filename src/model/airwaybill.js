const db = require("../utils/db");

const Airwaybill = class {
  constructor() {}

  viewall = async (userId, status) => {
    try {
      let sql = null;
      //and vwfh.processName not in (?)
      if (status == "Closed") {
        sql =
          "Select * from airwaybill awb inner join (select `abd`.`airwaybillId` AS `airwaybillId`,`abd`.`accountNumber` AS `accountNumber`,`abd`.`referenceName` AS `referenceName`,`abd`.`name` AS `name`,`abd`.`phoneNumber` AS `phoneNumber`,`abd`.`companyName` AS `companyName`,`abd`.`streetAddress` AS `streetAddress`,`abd`.`city` AS `city`,`abd`.`postalCode` AS `postalCode`,`abd`.`country` AS `country` from (`airwaybilldetail` `abd` join `airwaybilldetailtype` `abdt` on(`abd`.`airwaybillDetailTypeId` = `abdt`.`id`)) where `abdt`.`name` = 'Shipper') shipper on awb.id=shipper.airwaybillId inner join (select `abd`.`airwaybillId` AS `airwaybillIdR`,`abd`.`accountNumber` AS `accountNumberR`,`abd`.`referenceName` AS `referenceNameR`,`abd`.`name` AS `nameR`,`abd`.`phoneNumber` AS `phoneNumberR`,`abd`.`companyName` AS `companyNameR`,`abd`.`streetAddress` AS `streetAddresR`,`abd`.`city` AS `cityR`,`abd`.`postalCode` AS `postalCodeR`,`abd`.`country` AS `countryR` from (`airwaybilldetail` `abd` join `airwaybilldetailtype` `abdt` on(`abd`.`airwaybillDetailTypeId` = `abdt`.`id`)) where `abdt`.`name` = 'Recipient') Recipient on awb.id=Recipient.airwaybillIdR inner join (select `wfh`.`id` AS `idw`,`wfh`.`airwaybillId` AS `airwaybillIdw`,`wfh`.`workflowId` AS `workflowId`,`wfh`.`createdBy` AS `createdBy`,`wfh`.`creationDate` AS `creationDate`,`wfh`.`completedBy` AS `completedBy`,`wfh`.`completionDate` AS `completionDate`,`wfh`.`isActive` AS `isActive`,`wf`.`processName` AS `processName`,`wf`.`processDescription` AS `processDescription` from (`workflowhistory` `wfh` join `workflow` `wf` on(`wfh`.`workflowId` = `wf`.`id`)) where `wfh`.`isActive` = 1) wfht on awb.id=wfht.airwaybillIdw inner join airwaybilldriver awbd on awb.id=awbd.billId where awbd.driverId=? and wfht.processName  like ?";
      } else {
        sql =
          "Select * from airwaybill awb inner join (select `abd`.`airwaybillId` AS `airwaybillId`,`abd`.`accountNumber` AS `accountNumber`,`abd`.`referenceName` AS `referenceName`,`abd`.`name` AS `name`,`abd`.`phoneNumber` AS `phoneNumber`,`abd`.`companyName` AS `companyName`,`abd`.`streetAddress` AS `streetAddress`,`abd`.`city` AS `city`,`abd`.`postalCode` AS `postalCode`,`abd`.`country` AS `country` from (`airwaybilldetail` `abd` join `airwaybilldetailtype` `abdt` on(`abd`.`airwaybillDetailTypeId` = `abdt`.`id`)) where `abdt`.`name` = 'Shipper') shipper on awb.id=shipper.airwaybillId inner join (select `abd`.`airwaybillId` AS `airwaybillIdR`,`abd`.`accountNumber` AS `accountNumberR`,`abd`.`referenceName` AS `referenceNameR`,`abd`.`name` AS `nameR`,`abd`.`phoneNumber` AS `phoneNumberR`,`abd`.`companyName` AS `companyNameR`,`abd`.`streetAddress` AS `streetAddresR`,`abd`.`city` AS `cityR`,`abd`.`postalCode` AS `postalCodeR`,`abd`.`country` AS `countryR` from (`airwaybilldetail` `abd` join `airwaybilldetailtype` `abdt` on(`abd`.`airwaybillDetailTypeId` = `abdt`.`id`)) where `abdt`.`name` = 'Recipient') Recipient on awb.id=Recipient.airwaybillIdR inner join (select `wfh`.`id` AS `idw`,`wfh`.`airwaybillId` AS `airwaybillIdw`,`wfh`.`workflowId` AS `workflowId`,`wfh`.`createdBy` AS `createdBy`,`wfh`.`creationDate` AS `creationDate`,`wfh`.`completedBy` AS `completedBy`,`wfh`.`completionDate` AS `completionDate`,`wfh`.`isActive` AS `isActive`,`wf`.`processName` AS `processName`,`wf`.`processDescription` AS `processDescription` from (`workflowhistory` `wfh` join `workflow` `wf` on(`wfh`.`workflowId` = `wf`.`id`)) where `wfh`.`isActive` = 1) wfht on awb.id=wfht.airwaybillIdw inner join airwaybilldriver awbd on awb.id=awbd.billId where awbd.driverId=? and wfht.processName Not like ?";
      }
      //const result = await db.query(sql);
      const result = await db.query(sql, [userId, "%" + "Closed" + "%"]);
      //await db.pool.end();
      return result;
    } catch (err) {
      return err.message;
    }
  };

  getDetail = async (id) => {};
  updateDetails = async (id, pieces, volumn, actualWeight, userId) => {
    try {
      console.log(">:____________");
      console.log(":>Updating Details:");
      let sql =
        "Update airwaybill set noOfPieces=?,volWeight=?, actualWeight=? where id=?";
      const result = await db.query(sql, [pieces, volumn, actualWeight, id]);
      if (result.affectedRows) {
        console.log(":>Updating Details Done");
        let result2 = await this.findOrder(id, userId);
        if (result2) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (err) {
      console.log("Error:> Error Occupied while Update Details ", err.log);
      return false;
    }
  };

  updateWorkFlowHistory = async (userId, id) => {
    try {
      console.log(":>Update history Id for ", id);
      let result = await db.query(
        "Update workflowhistory set completedBy=?,isActive=0 where id=?",
        [userId, id]
      );
      if (result.affectedRows) {
        console.log(":> History Updated ....");
        return true;
      } else {
        console.log(":>History not updated");
        return false;
      }
    } catch (error) {
      console.log("Error:> ", error.message);
      return false;
    }
  };
  findOrder = async (airwaybillId, userId) => {
    try {
      console.log(">:Finding history started");
      let sql =
        "Select * from workflowhistory where airwaybillId=? and isActive=1";
      const result = await db.query(sql, [airwaybillId]);

      if (result.length > 0) {
        console.log(":>", result.length, " Items Found.");
        let id = null;
        let workflowId = null;
        result.forEach((row) => {
          id = row["id"];
          workflowId = row["workflowId"];
          console.log(":>", id, ":id at workflow ", workflowId);

          //Update the
        });
        console.log(":>", id, ":id at workflow ", workflowId);

        if (id != null) {
          //Move the
          let updateRes = await this.updateWorkFlowHistory(userId, id);
          console.log(":>Stage 3 :");
          if (updateRes) {
            let rse = await this.moveToNextStep(
              airwaybillId,
              userId,
              workflowId
            );
            return rse;
          } else {
            console.log(":>Stage 2 never Updated");
            return false;
          }
        }
      } else {
        console.log(":>No Item found");
        return false;
      }
      return result;
    } catch (err) {
      console.log("Error: > ", err.message);
      return err.message;
    }
  };
  moveToNextStep = async (airwaybillId, userId, currentWorkflowId) => {
    try {
      console.log(":>Lets move to the  step :", currentWorkflowId);
      let sql = "SELECT * from workflow where id = ?";
      let result = await db.query(sql, [currentWorkflowId]);
      if (result.length > 0) {
        console.log(":>Item Found in the Workflow table");
        let nextId = null;
        result.forEach((row) => {
          nextId = row["NextProcessId"];
        });

        console.log(":>Stage 4 The next Process is ", nextId);
        let res = await this.createNewStep(airwaybillId, userId, nextId);
        if (res) {
          return true;
        }
      } else {
        console.log(":>No the data updated");
        return false;
      }
    } catch (err) {
      console.log("Error: ", err.message);
      return false;
    }
  };

  createNewStep = async (airwaybillId, userId, nextId) => {
    try {
      console.log(":>Lets Nove to the Next Stages Now");
      let sql =
        "Insert into workflowhistory (airwaybillId,workflowId,createdBy,isActive)values(?,?,?,1)";
      let result2 = await db.query(sql, [airwaybillId, nextId, userId]);
      if (result2.affectedRows) {
        console.log(":> Its all Successful Ssebo");
        return true;
      } else {
        return false;
      }
    } catch (err) {
      console.log("Error: ", err.message);
      return false;
    }
  };

  currentTime = () => {
    let date_ob = new Date();
    // current hours
    let hours =
      date_ob.getHours() > 9 ? date_ob.getHours() : "0" + date_ob.getHours();
    // current minutes
    let minutes =
      date_ob.getMinutes() > 9
        ? date_ob.getMinutes()
        : "0" + date_ob.getMinutes();
    // current seconds
    let seconds =
      date_ob.getSeconds() > 9
        ? date_ob.getSeconds()
        : "0" + date_ob.getSeconds();
    return hours + ":" + minutes + ":" + seconds;
  };

  today = () => {
    let date_ob = new Date();
    let date =
      date_ob.getDate() > 9 ? date_ob.getDate() : "0" + date_ob.getDate();
    let month =
      date_ob.getMonth() + 1 > 9
        ? date_ob.getMonth() + 1
        : "0" + date_ob.getMonth() + 1;
    let year = date_ob.getFullYear();
    return year + "-" + month + "-" + date;
  };

  //check if the driver location is need to record query very 15 minutes
  locateDriver = async (driverId) => {
    console.log(":>Let check if the driver s is in the Transit query");
    try {
      let sql = "Select * from vwshipmentmonitor where driverId=?";
      console.log(":>Let check if the driver is in the Transit query");
      let resultSet = await db.query(sql, [driverId]);
      return resultSet;
    } catch (error) {
      console.log(">", error.message);
      return [];
    }
  };
};

module.exports = Airwaybill;
