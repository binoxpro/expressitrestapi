const db = require("../utils/db");
const VerifyCode = require("./verifycode");
const MAuthentication = require("./mAuthentication");
const MobileUser = class {
  constructor() {
    this.id = this.id;
    this.firstName = null;
    this.lastName = null;
    this.mobileNumber = null;
    this.status = null;
  }

  edit = async () => {
    const result = await db.query(
      "Update `mobileuser` set status='Verified' where id=? ",
      [this.id]
    );
    let message = false;
    if (result.affectedRows) {
      message = true;
    }
    //console.log(message);

    return message;
  };

  find = async () => {
    //let status = null;
    try {
      console.log(
        ":> Finding the mobile user of mobile Number ",
        this.mobileNumber
      );
      const result = await db.query(
        "Select * from mobileuser where mobileNumber =? ",
        [this.mobileNumber]
      );
      console.log(":>Number of record is ", result.length);
      if (result.length > 0) {
        console.log(":> User found ");
        result.forEach((row) => {
          this.loadUser(row);
        });
      } else {
        console.log(":> User Not Found ");
        this.status;
      }
    } catch (err) {
      console.log("Error", err.message);
      return err.message;
    }

    //return status;
  };
  newUserAction = async (pword) => {
    let re = await this.find();

    console.log(">:User status is ", this.status, this.id);
    if (this.status === "Verify") {
      //Send Code
      let verifyCode = new VerifyCode();
      verifyCode.userId = this.id;
      let message = await verifyCode.add();
      if (message) {
        let status = await verifyCode.sendSmsCode(this);
        if (status === "Sent") {
          //console.log("Sms Sent ", this.id);
          return { status: this.status, userId: this.id, message: "OK" };
        } else {
          return { status: this.status, userId: this.id, message: status };
        }
      } else {
        return { status: this.status, userId: -1, message: "error" };
      }
    } else if (this.status === "Verified") {
      //Login user
      var mAuth = new MAuthentication();
      mAuth.userId = this.id;
      mAuth.pword = pword;
      //mAuth.getPword();
      let authenticStatus = await mAuth.login();
      if (authenticStatus) {
        return {
          status: "Authenticated",
          userId: this.id,
          message: "Login Status",
        };
      } else {
        return {
          status: "NotAuthenticated",
          userId: this.id,
          message: "Login Status",
        };
      }
    } else {
      //Cannot Login User
      return {
        status: "Not Found",
        userId: -1,
        message: "Please contact Express It Logistic",
      };
    }
  };

  loadUser = (row) => {
    this.id = row["id"];
    this.firstName = row["firstName"];
    this.lastName = row["lastName"];
    this.mobileNumber = row["mobileNumber"];
    this.status = row["status"];
  };
};

module.exports = { MobileUser };
