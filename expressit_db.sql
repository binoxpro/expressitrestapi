-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2021 at 10:22 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `expressit_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `indexOrder` int(11) NOT NULL,
  `isActive` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `name`, `indexOrder`, `isActive`) VALUES
(5, 'Configuration', 4, 1),
(24, 'Schemes', 5, 1),
(28, 'User Management', 3, 1),
(37, 'undefined', 20, 1),
(38, 'Table Maintance', 2, 1),
(39, 'Manage Airway Bill', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `airwaybill`
--

CREATE TABLE `airwaybill` (
  `id` varchar(150) NOT NULL,
  `origin` varchar(250) NOT NULL,
  `captureDate` date NOT NULL,
  `destination` varchar(250) NOT NULL,
  `contents` varchar(250) NOT NULL,
  `customsValue` varchar(250) DEFAULT NULL,
  `insuranceValue` varchar(250) DEFAULT NULL,
  `dimension` varchar(150) DEFAULT NULL,
  `noOfPieces` int(15) NOT NULL,
  `volWeight` float DEFAULT NULL,
  `actualWeight` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `airwaybill`
--

INSERT INTO `airwaybill` (`id`, `origin`, `captureDate`, `destination`, `contents`, `customsValue`, `insuranceValue`, `dimension`, `noOfPieces`, `volWeight`, `actualWeight`) VALUES
('E21100001', 'Mukono', '2021-10-12', 'Entebbe', 'Blood Samples', '', '', '15 Boxs', 15, 250, 0),
('E21100002', 'Gulu', '2021-10-12', 'Entebbe', 'Blood Samples', '', '', '15*6', 15, 45, 0),
('E21100003', 'Lira', '2021-10-13', 'Tororo', 'tetete', '', '', '5*7', 10, 0, 0),
('E21100004', 'Gulu', '2021-10-13', 'Entebbe', 'Blood Samples', '', '', 'Box', 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `airwaybillcharge`
--

CREATE TABLE `airwaybillcharge` (
  `id` int(15) NOT NULL,
  `airwaybillId` int(15) NOT NULL,
  `airwaybillTemplateId` int(15) NOT NULL,
  `amount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `airwaybillcharge`
--

INSERT INTO `airwaybillcharge` (`id`, `airwaybillId`, `airwaybillTemplateId`, `amount`) VALUES
(1, 10300, 1, 0),
(2, 10300, 2, 0),
(3, 10300, 3, 0),
(4, 10300, 4, 0),
(5, 1, 1, 0),
(6, 1, 2, 0),
(7, 1, 3, 0),
(8, 1, 4, 0),
(9, 10000, 1, 0),
(10, 10000, 2, 0),
(11, 10000, 3, 0),
(12, 10000, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `airwaybillchargetemplate`
--

CREATE TABLE `airwaybillchargetemplate` (
  `id` int(15) NOT NULL,
  `templateName` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `airwaybillchargetemplate`
--

INSERT INTO `airwaybillchargetemplate` (`id`, `templateName`) VALUES
(1, 'Freight Charge'),
(2, 'Other'),
(3, 'Insurance'),
(4, 'SAME-DAY SURCHARGE');

-- --------------------------------------------------------

--
-- Table structure for table `airwaybilldetail`
--

CREATE TABLE `airwaybilldetail` (
  `id` int(15) NOT NULL,
  `airwaybillId` varchar(150) NOT NULL,
  `airwaybillDetailTypeId` varchar(150) NOT NULL,
  `accountNumber` varchar(150) NOT NULL,
  `referenceName` varchar(150) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `phoneNumber` varchar(150) NOT NULL,
  `companyName` varchar(150) NOT NULL,
  `streetAddress` varchar(150) NOT NULL,
  `city` varchar(150) NOT NULL,
  `postalCode` varchar(150) DEFAULT NULL,
  `country` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `airwaybilldetail`
--

INSERT INTO `airwaybilldetail` (`id`, `airwaybillId`, `airwaybillDetailTypeId`, `accountNumber`, `referenceName`, `name`, `phoneNumber`, `companyName`, `streetAddress`, `city`, `postalCode`, `country`) VALUES
(1, 'E21100001', '1', '2560003', 'TR0001089', 'James Yiga', '0781587081', 'Sybrin', 'Lumumba', 'Kla', '', 'Uganda'),
(2, 'E21100001', '2', '', NULL, 'Dr Joan Acen', '0781587081', 'RHC', 'Entebbe', 'Entebbe', NULL, NULL),
(3, 'E21100002', '1', '2560003', '', 'Yiga James', '0781587081', 'Sybrin', 'Lumumba', 'Kla', '256', 'Uganda'),
(4, 'E21100002', '2', '', NULL, 'Peter Miles', '0781587081', 'HTS', 'Entebbe', 'Entebbe', NULL, NULL),
(5, 'E21100003', '1', '2560003', '', 'Yiga James', '0781587081', 'Sybrin', 'Lumumba', 'Kla', '', 'Uganda'),
(6, 'E21100003', '2', '', NULL, 'Ken', '0781587081', 'TIN', 'Ka', 'klll', NULL, NULL),
(7, 'E21100004', '1', '2560003', 'TR009', 'Moses', '0781587081', 'Sybrin', 'Lumumba', 'Kla', '', 'Uganda'),
(8, 'E21100004', '2', '', NULL, 'Dr Okello', '0781587081', 'HRC', 'Entebbe', 'Ebt', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `airwaybilldetailtype`
--

CREATE TABLE `airwaybilldetailtype` (
  `id` int(15) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `airwaybilldetailtype`
--

INSERT INTO `airwaybilldetailtype` (`id`, `name`) VALUES
(1, 'Shipper'),
(2, 'Recipient');

-- --------------------------------------------------------

--
-- Table structure for table `airwaybilldriver`
--

CREATE TABLE `airwaybilldriver` (
  `id` int(15) NOT NULL,
  `billId` varchar(150) NOT NULL,
  `driverId` varchar(150) DEFAULT NULL,
  `isActive` int(1) NOT NULL,
  `comment` varchar(150) DEFAULT NULL,
  `modificationDate` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `airwaybilldriver`
--

INSERT INTO `airwaybilldriver` (`id`, `billId`, `driverId`, `isActive`, `comment`, `modificationDate`) VALUES
(1, 'E21100001', '{81C65E28-F021-6319-CBE6-EDA4090FB9EA}', 1, 'testing', NULL),
(2, 'E21100002', '{81C65E28-F021-6319-CBE6-EDA4090FB9EA}', 1, '', NULL),
(3, 'E21100001', '{81C65E28-F021-6319-CBE6-EDA4090FB9EA}', 1, 'text', NULL),
(4, 'E21100003', '{81C65E28-F021-6319-CBE6-EDA4090FB9EA}', 1, 'text gg', NULL),
(5, 'E21100004', '{81C65E28-F021-6319-CBE6-EDA4090FB9EA}', 1, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `airwaybillinstruction`
--

CREATE TABLE `airwaybillinstruction` (
  `id` int(15) NOT NULL,
  `airwayBillId` varchar(150) DEFAULT NULL,
  `instruction` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `airwaybillinstruction`
--

INSERT INTO `airwaybillinstruction` (`id`, `airwayBillId`, `instruction`) VALUES
(1, '10300', 'Testing'),
(2, '10300', 'Keep frozen'),
(3, '0', 'Keep Frozen'),
(4, 'E21100001', 'Keep Frozen'),
(5, 'E21100002', 'Keep Frozen'),
(6, 'E21100004', 'Keep frozen');

-- --------------------------------------------------------

--
-- Table structure for table `apiroute`
--

CREATE TABLE `apiroute` (
  `id` int(15) NOT NULL,
  `userId` varchar(150) NOT NULL,
  `creationTime` datetime NOT NULL DEFAULT current_timestamp(),
  `responseTime` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `actionQuery` varchar(150) DEFAULT NULL,
  `token` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `applyrequirement`
--

CREATE TABLE `applyrequirement` (
  `id` int(15) NOT NULL,
  `requirement` text NOT NULL,
  `amount` int(15) NOT NULL,
  `documentId` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `casecontent`
--

CREATE TABLE `casecontent` (
  `id` int(15) NOT NULL,
  `content` text NOT NULL,
  `caseId` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `casecontent`
--

INSERT INTO `casecontent` (`id`, `content`, `caseId`) VALUES
(3, 'PHA+PGVtPkFsbCBvZiBtZSBqb2huIGxlZ2VuZCBkb3dubG9hZDwvZW0+IGF1ZGlvIDxlbT5tcDMgYWxsIG9mIG1lIGpvaG4gbGVnZW5kPC9lbT4gMTI4a2JwcyA8ZW0+YWxsIG9mIG1lIGpvaG4gbGVnZW5kPC9lbT4gZnVsbCBocSAzMjBrYnBzIDxlbT5hbGwgb2YgbWUgam9obiBsZWdlbmQgbXAzPC9lbT4uIFdhdGNoIHRoZSB2aWRlbyBmb3ImbmJzcDsuLi48L3A+DQo=', 3),
(4, 'PHA+PGVtPkFsbCBvZiBtZSBqb2huIGxlZ2VuZCBkb3dubG9hZDwvZW0+IGF1ZGlvIDxlbT5tcDMgYWxsIG9mIG1lIGpvaG4gbGVnZW5kPC9lbT4gMTI4a2JwcyA8ZW0+YWxsIG9mIG1lIGpvaG4gbGVnZW5kPC9lbT4gZnVsbCBocSAzMjBrYnBzIDxlbT5hbGwgb2YgbWUgam9obiBsZWdlbmQgbXAzPC9lbT4uIFdhdGNoIHRoZSB2aWRlbyBmb3ImbmJzcDsuLi48L3A+DQo=', 1);

-- --------------------------------------------------------

--
-- Table structure for table `casehearing`
--

CREATE TABLE `casehearing` (
  `id` int(15) NOT NULL,
  `caseId` int(15) NOT NULL,
  `Date` date NOT NULL,
  `isActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `casejudge`
--

CREATE TABLE `casejudge` (
  `id` int(15) NOT NULL,
  `judgeId` int(15) NOT NULL,
  `caseId` int(15) NOT NULL,
  `isActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `casejudge`
--

INSERT INTO `casejudge` (`id`, `judgeId`, `caseId`, `isActive`) VALUES
(1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `casenumber`
--

CREATE TABLE `casenumber` (
  `id` int(15) NOT NULL,
  `caseNo` varchar(150) NOT NULL,
  `applicant` varchar(250) NOT NULL,
  `respondent` varchar(250) NOT NULL,
  `filingDate` date NOT NULL,
  `caseHeadline` varchar(150) DEFAULT NULL,
  `hearingDate` date DEFAULT NULL,
  `status` varchar(15) NOT NULL,
  `cite` varchar(150) DEFAULT NULL,
  `rulingDate` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `casepge`
--

CREATE TABLE `casepge` (
  `id` int(15) NOT NULL,
  `caseId` varchar(50) NOT NULL,
  `path` text NOT NULL,
  `fileName` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `caseruling`
--

CREATE TABLE `caseruling` (
  `id` int(15) NOT NULL,
  `content` text NOT NULL,
  `creationDate` datetime DEFAULT current_timestamp(),
  `isActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cf`
--

CREATE TABLE `cf` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checklist`
--

CREATE TABLE `checklist` (
  `id` int(15) NOT NULL,
  `questionNo` int(15) NOT NULL,
  `questionWord` text NOT NULL,
  `priorty` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `checklist`
--

INSERT INTO `checklist` (`id`, `questionNo`, `questionWord`, `priorty`) VALUES
(1, 1, 'Do you Invoice your client?', 'high'),
(2, 2, 'Do you have a TIN Number?', 'high'),
(3, 3, 'Do You offer a service?', 'Moderate'),
(4, 4, 'Do you track invoice expense?', 'low'),
(5, 5, 'Do you have a computer?', 'high'),
(6, 6, 'Proving 1+1=2', 'High');

-- --------------------------------------------------------

--
-- Table structure for table `checklistanswer`
--

CREATE TABLE `checklistanswer` (
  `id` int(15) NOT NULL,
  `answer` varchar(50) NOT NULL,
  `value` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `checklistanswer`
--

INSERT INTO `checklistanswer` (`id`, `answer`, `value`) VALUES
(1, 'NO', 25),
(2, 'YES', 100),
(3, 'I DONOT KNOW', 50),
(4, 'DOES NOT APPLY', 10);

-- --------------------------------------------------------

--
-- Table structure for table `codebuilder`
--

CREATE TABLE `codebuilder` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `collectionsite`
--

CREATE TABLE `collectionsite` (
  `id` int(15) NOT NULL,
  `siteName` varchar(250) NOT NULL,
  `district` varchar(150) NOT NULL,
  `lat` varchar(150) NOT NULL,
  `lng` varchar(150) NOT NULL,
  `isActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `collectionsite`
--

INSERT INTO `collectionsite` (`id`, `siteName`, `district`, `lat`, `lng`, `isActive`) VALUES
(4, 'Mpiga', 'mpiga uganda', '0.2273528', '32.3249236', 1),
(5, 'Entebbe', 'Entebbe Research center uganda', '0.07569189999999999', '32.4574649', 1);

-- --------------------------------------------------------

--
-- Table structure for table `configurationsetting`
--

CREATE TABLE `configurationsetting` (
  `id` int(11) NOT NULL,
  `systemName` varchar(50) NOT NULL,
  `systemValue` varchar(250) DEFAULT NULL,
  `mask` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configurationsetting`
--

INSERT INTO `configurationsetting` (`id`, `systemName`, `systemValue`, `mask`) VALUES
(12, 'Company Name', 'Express IT Logistic', 1),
(13, 'Company logo', 'logox.jpeg', 1),
(14, 'address', 'Rumee House 3dr Floor ,Plot 19 Lumumba Avenue - Nakasero, 74638, Kampala- Uganda', 1),
(20, 'app Name', 'Airway Bill Tracker', 1),
(22, 'company contact', '+256-756-270818 +256-776-270818', 1),
(32, 'webserver', 'http://localhost/expressit.org', 1),
(33, 'generalEmail.com', 'xonibsoftware@code360ds.com', 1),
(34, 'mailServer', 'p3plcpnl0579.prod.phx3.secureserver.net', 1),
(35, 'userName', 'xonibsoftware@code360ds.com', 1),
(36, 'password', 'h*p81!0}AoMq', 1);

-- --------------------------------------------------------

--
-- Table structure for table `consigeeaccount`
--

CREATE TABLE `consigeeaccount` (
  `id` varchar(13) NOT NULL,
  `company` varchar(150) NOT NULL,
  `address` varchar(150) NOT NULL,
  `phoneNumber` varchar(150) NOT NULL,
  `city` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `consigeeaccount`
--

INSERT INTO `consigeeaccount` (`id`, `company`, `address`, `phoneNumber`, `city`) VALUES
('2560001', 'r', '', 'r', 'r'),
('2560002', 'f', 'f', 'f', 'f'),
('2560003', 'Sybrin', 'Lumumba', '0781587081', 'Kla');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(15) NOT NULL,
  `countryName` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `countryName`) VALUES
(1, 'Uganda'),
(2, 'Kenya'),
(3, 'TZ'),
(4, 'Rwanda'),
(5, 'Burindi'),
(6, 'South Sudan'),
(7, 'EAC Secretariat');

-- --------------------------------------------------------

--
-- Table structure for table `courtclerk`
--

CREATE TABLE `courtclerk` (
  `id` int(15) NOT NULL,
  `town` varchar(150) NOT NULL,
  `tel` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `registryId` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `courtclerk`
--

INSERT INTO `courtclerk` (`id`, `town`, `tel`, `email`, `registryId`) VALUES
(4, 'rreee', 'eeeee', 'eewwddff', 1);

-- --------------------------------------------------------

--
-- Table structure for table `democase`
--

CREATE TABLE `democase` (
  `id` int(15) NOT NULL,
  `creationDate` datetime DEFAULT current_timestamp(),
  `userId` varchar(150) NOT NULL,
  `reviewed` varchar(15) DEFAULT NULL,
  `reviewedBy` varchar(150) NOT NULL,
  `reviewDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `comment` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `democase`
--

INSERT INTO `democase` (`id`, `creationDate`, `userId`, `reviewed`, `reviewedBy`, `reviewDate`, `comment`) VALUES
(1, '2021-06-12 15:47:52', '{67EBA0E9-6F38-0F8E-644B-B07B6B5C73D9}', 'Closed', '', '2021-06-14 20:45:14', ''),
(2, '2021-06-15 17:02:50', '{67EBA0E9-6F38-0F8E-644B-B07B6B5C73D9}', 'Closed', '', '2021-06-16 08:23:30', ''),
(3, '2021-06-16 19:05:31', '{67EBA0E9-6F38-0F8E-644B-B07B6B5C73D9}', 'Closed', '', '2021-06-25 13:47:41', ''),
(4, '2021-06-25 13:47:50', '{67EBA0E9-6F38-0F8E-644B-B07B6B5C73D9}', 'Closed', '', '2021-06-25 17:24:33', ''),
(5, '2021-06-25 17:27:32', '{67EBA0E9-6F38-0F8E-644B-B07B6B5C73D9}', 'Closed', '', '2021-06-25 17:37:06', ''),
(6, '2021-06-27 17:01:13', '{67EBA0E9-6F38-0F8E-644B-B07B6B5C73D9}', 'Closed', '', '2021-07-19 08:51:48', ''),
(7, '2021-06-27 20:30:39', '4', 'Started', '', NULL, ''),
(8, '2021-06-27 20:33:02', '4', 'Started', '', NULL, ''),
(9, '2021-07-19 08:51:54', '{67EBA0E9-6F38-0F8E-644B-B07B6B5C73D9}', 'Closed', '', '2021-08-22 13:51:13', ''),
(10, '2021-08-11 22:25:26', '38418485-5552-4290-a196-b67beef99b03', 'Started', '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `democaseanswer`
--

CREATE TABLE `democaseanswer` (
  `id` int(15) NOT NULL,
  `demoCaseId` int(15) NOT NULL,
  `qnsId` int(15) NOT NULL,
  `ansId` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `democaseanswer`
--

INSERT INTO `democaseanswer` (`id`, `demoCaseId`, `qnsId`, `ansId`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 8),
(3, 1, 3, 10),
(4, 1, 4, 13),
(5, 1, 5, 17),
(6, 1, 6, 20),
(7, 2, 1, 1),
(8, 2, 2, 8),
(9, 2, 4, 13),
(10, 2, 5, 16),
(11, 2, 6, 20),
(12, 2, 3, 11),
(13, 3, 1, 1),
(14, 3, 2, 8),
(15, 3, 3, 11),
(16, 3, 4, 13),
(17, 3, 5, 16),
(18, 3, 6, 20),
(19, 4, 1, 1),
(20, 4, 2, 8),
(21, 4, 3, 11),
(22, 4, 4, 13),
(23, 4, 5, 16),
(24, 4, 6, 20),
(25, 5, 1, 1),
(26, 5, 2, 9),
(27, 5, 4, 13),
(28, 5, 5, 16),
(29, 5, 6, 20),
(30, 6, 1, 1),
(31, 6, 2, 8),
(32, 6, 3, 10),
(33, 6, 4, 13),
(34, 6, 5, 16),
(35, 6, 6, 20),
(36, 9, 1, 1),
(37, 9, 2, 8),
(38, 9, 3, 11),
(39, 9, 4, 15),
(40, 9, 6, 20);

-- --------------------------------------------------------

--
-- Table structure for table `democasecontact`
--

CREATE TABLE `democasecontact` (
  `id` int(15) NOT NULL,
  `demoCaseId` int(15) NOT NULL,
  `subject` varchar(250) NOT NULL,
  `msg` text NOT NULL,
  `creationDate` datetime DEFAULT current_timestamp(),
  `status` varchar(50) DEFAULT 'NEW'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `democasecontact`
--

INSERT INTO `democasecontact` (`id`, `demoCaseId`, `subject`, `msg`, `creationDate`, `status`) VALUES
(1, 2, 'Testing ', 'Testing for less', '2021-06-16 08:21:43', 'OPENED'),
(2, 2, 'TEST', 'tESTING', '2021-06-16 08:23:29', 'OPENED'),
(3, 4, 'Testing', 'Testing ', '2021-06-25 17:24:33', 'OPENED');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(15) NOT NULL,
  `documentName` varchar(150) NOT NULL,
  `documentDetail` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `documentName`, `documentDetail`) VALUES
(1, 'Test Document', 'Testimg the Systst'),
(2, 'Test Doc 2', 'Testing the System can handle the Document'),
(3, 'Test Doc 3', 'tESTING'),
(4, 'Test James doc', 'tttttttt gggg');

-- --------------------------------------------------------

--
-- Table structure for table `documentaccessmode`
--

CREATE TABLE `documentaccessmode` (
  `id` int(15) NOT NULL,
  `mode` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `documentaccessmode`
--

INSERT INTO `documentaccessmode` (`id`, `mode`) VALUES
(1, 'Free'),
(2, 'Buy'),
(3, 'Subscription');

-- --------------------------------------------------------

--
-- Table structure for table `documentpge`
--

CREATE TABLE `documentpge` (
  `id` int(15) NOT NULL,
  `path` varchar(250) NOT NULL,
  `hashVal` varchar(250) NOT NULL,
  `isActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `documentpge`
--

INSERT INTO `documentpge` (`id`, `path`, `hashVal`, `isActive`) VALUES
(1, '../public/lib/default/document(4).pdf', '0', 1),
(2, '../public/lib/default/qoutation.pdf', '0', 1),
(3, '../public/lib/default/R2.pdf', '0', 1),
(4, '../public/lib/default/Restaurant Inventory Checklist.pdf', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eacjdivison`
--

CREATE TABLE `eacjdivison` (
  `id` int(10) NOT NULL,
  `divisionName` varchar(150) NOT NULL,
  `isActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `eacjdivison`
--

INSERT INTO `eacjdivison` (`id`, `divisionName`, `isActive`) VALUES
(1, 'Appellate Division', 1),
(2, 'First Instance Division', 1);

-- --------------------------------------------------------

--
-- Table structure for table `entity`
--

CREATE TABLE `entity` (
  `id` int(11) NOT NULL,
  `tableName` varchar(50) NOT NULL,
  `inSync` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entity`
--

INSERT INTO `entity` (`id`, `tableName`, `inSync`) VALUES
(1, 'activity', 1),
(2, 'apiroute', 1),
(3, 'cf', 1),
(4, 'codebuilder', 1),
(5, 'configurationsetting', 1),
(6, 'entity', 1),
(7, 'entityfield', 1),
(8, 'entityform', 1),
(9, 'guitoolbox', 1),
(10, 'home', 1),
(11, 'listhandle', 1),
(12, 'rolemanagement', 1),
(13, 'subactivity', 1),
(14, 'systemuser', 1),
(15, 'toolbox', 1),
(16, 'user', 1),
(17, 'useroftype', 1),
(18, 'userpge', 1),
(19, 'usertype', 1),
(20, 'usertyperole', 1),
(21, 'judgeposition', 1),
(22, 'eacjdivison', 1),
(23, 'judge', 1),
(24, 'judgepge', 1),
(25, 'errorhandler', 1),
(26, 'vwjudge', 1),
(27, 'country', 1),
(28, 'registry', 1),
(29, 'courtclerk', 1),
(30, 'casenumber', 1),
(31, 'casejudge', 1),
(32, 'caseruling', 1),
(33, 'casecontent', 1),
(34, 'casepge', 1),
(35, 'casehearing', 1),
(36, 'searchword', 1),
(37, 'stage', 1),
(38, 'document', 1),
(39, 'documentpge', 1),
(40, 'applyrequirement', 1),
(41, 'panel', 1),
(42, 'signup', 1),
(43, 'checklistanswer', 1),
(44, 'checklist', 1),
(45, 'questionanswer', 1),
(46, 'userscore', 1),
(47, 'democase', 1),
(48, 'democaseanswer', 1),
(49, 'rankingval', 1),
(50, 'statu', 1),
(51, 'verifycode', 1),
(52, 'vwfactor', 1),
(53, 'vwweightfactor', 1),
(54, 'procdoc', 1),
(55, 'documentaccessmode', 1),
(56, 'proc', 1),
(57, 'democasecontact', 1),
(58, 'collectionsite', 1),
(59, 'shipperdetail', 1),
(60, 'mobileuser', 1),
(61, 'mauthentication', 1),
(62, 'workflow', 1),
(63, 'workflowhistory', 1),
(64, 'airwaybill', 1),
(65, 'airwaybilldetailtype', 1),
(66, 'airwaybilldetail', 1),
(67, 'airwaybillinstruction', 1),
(68, 'airwaybillchargetemplate', 1),
(69, 'airwaybillcharge', 1),
(70, 'consigeeaccount', 1),
(71, 'airwaybilldriver', 1);

-- --------------------------------------------------------

--
-- Table structure for table `entityfield`
--

CREATE TABLE `entityfield` (
  `id` int(11) NOT NULL,
  `fieldName` varchar(150) NOT NULL,
  `fieldType` varchar(150) NOT NULL,
  `fieldLength` int(11) NOT NULL,
  `fieldConstraint` varchar(150) NOT NULL,
  `canBeNull` tinyint(1) NOT NULL,
  `entityId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entityfield`
--

INSERT INTO `entityfield` (`id`, `fieldName`, `fieldType`, `fieldLength`, `fieldConstraint`, `canBeNull`, `entityId`) VALUES
(1, 'id', 'int', 11, 'auto_increment', 0, 1),
(2, 'name', 'varchar', 50, '', 1, 1),
(3, 'indexOrder', 'int', 11, '', 0, 1),
(4, 'isActive', 'tinyint', 1, '', 1, 1),
(5, 'id', 'int', 15, 'auto_increment', 0, 2),
(6, 'userId', 'varchar', 150, '', 0, 2),
(7, 'creationTime', 'datetime', 0, '', 0, 2),
(8, 'responseTime', 'datetime', 0, 'on update current_timestamp()', 1, 2),
(9, 'actionQuery', 'varchar', 150, '', 1, 2),
(10, 'token', 'varchar', 150, '', 1, 2),
(11, 'id', 'int', 11, '', 0, 3),
(12, 'id', 'int', 11, '', 0, 4),
(13, 'id', 'int', 11, 'auto_increment', 0, 5),
(14, 'systemName', 'varchar', 50, '', 0, 5),
(15, 'systemValue', 'varchar', 250, '', 1, 5),
(16, 'mask', 'int', 11, '', 0, 5),
(17, 'id', 'int', 11, 'auto_increment', 0, 6),
(18, 'tableName', 'varchar', 50, '', 0, 6),
(19, 'inSync', 'tinyint', 1, '', 0, 6),
(20, 'id', 'int', 11, 'auto_increment', 0, 7),
(21, 'fieldName', 'varchar', 150, '', 0, 7),
(22, 'fieldType', 'varchar', 150, '', 0, 7),
(23, 'fieldLength', 'int', 11, '', 0, 7),
(24, 'fieldConstraint', 'varchar', 150, '', 0, 7),
(25, 'canBeNull', 'tinyint', 1, '', 0, 7),
(26, 'entityId', 'int', 11, '', 0, 7),
(27, 'colName', 'varchar', 150, '', 0, 8),
(28, 'controlName', 'varchar', 150, '', 1, 8),
(29, 'labelName', 'varchar', 50, '', 0, 8),
(30, 'url', 'varchar', 250, '', 1, 8),
(31, 'txtVal', 'varchar', 150, '', 1, 8),
(32, 'valval', 'varchar', 150, '', 1, 8),
(33, 'isActive', 'int', 11, '', 1, 8),
(34, 'tableNme', 'varchar', 150, '', 0, 8),
(35, 'orderIn', 'int', 11, '', 1, 8),
(36, 'positionIn', 'int', 11, '', 1, 8),
(37, 'isRequired', 'tinyint', 1, '', 0, 8),
(38, 'id', 'int', 15, '', 0, 9),
(39, 'guiToolName', 'varchar', 150, '', 0, 9),
(40, 'openTag', 'varchar', 250, '', 0, 9),
(41, 'closeTag', 'varchar', 250, '', 1, 9),
(42, 'dispayHtml', 'varchar', 250, '', 1, 9),
(43, 'defaultClass', 'varchar', 250, '', 1, 9),
(44, 'id', 'int', 11, '', 0, 10),
(45, 'id', 'int', 11, 'auto_increment', 0, 11),
(46, 'idVal', 'varchar', 150, '', 0, 11),
(47, 'textVal', 'varchar', 150, '', 0, 11),
(48, 'listName', 'varchar', 150, '', 0, 11),
(49, 'isSync', 'int', 1, '', 0, 11),
(50, 'id', 'int', 11, 'auto_increment', 0, 12),
(51, 'roleId', 'int', 11, '', 0, 12),
(52, 'userId', 'varchar', 250, '', 0, 12),
(53, 'id', 'int', 11, 'auto_increment', 0, 13),
(54, 'name', 'varchar', 50, '', 0, 13),
(55, 'link', 'varchar', 225, '', 1, 13),
(56, 'icon', 'varchar', 50, '', 1, 13),
(57, 'activityId', 'int', 11, '', 0, 13),
(58, 'isActive', 'tinyint', 1, '', 1, 13),
(59, 'orderIndex', 'int', 11, '', 1, 13),
(60, 'id', 'varchar', 250, '', 0, 14),
(61, 'firstName', 'varchar', 200, '', 0, 14),
(62, 'lastName', 'varchar', 200, '', 0, 14),
(63, 'dob', 'date', 0, '', 0, 14),
(64, 'contact', 'varchar', 50, '', 1, 14),
(65, 'email', 'varchar', 200, '', 1, 14),
(66, 'isActive', 'tinyint', 1, '', 1, 14),
(67, 'username', 'varchar', 50, '', 1, 14),
(68, 'password', 'varchar', 255, '', 1, 14),
(69, 'id', 'int', 11, 'auto_increment', 0, 15),
(70, 'toolboxName', 'varchar', 150, '', 0, 15),
(71, 'id', 'varchar', 50, '', 0, 16),
(72, 'firstName', 'varchar', 50, '', 0, 16),
(73, 'lastName', 'varchar', 50, '', 0, 16),
(74, 'mobileNumber', 'varchar', 20, '', 0, 16),
(75, 'email', 'varchar', 50, '', 1, 16),
(76, 'creationDate', 'datetime', 0, '', 0, 16),
(77, 'statusId', 'int', 10, '', 1, 16),
(78, 'userId', 'varchar', 50, '', 0, 17),
(79, 'userTypeId', 'int', 11, '', 0, 17),
(80, 'isActive', 'tinyint', 1, '', 0, 17),
(81, 'id', 'varchar', 50, '', 0, 18),
(82, 'folder', 'varchar', 50, '', 0, 18),
(83, 'image', 'int', 50, '', 0, 18),
(84, 'isactive', 'tinyint', 1, '', 0, 18),
(85, 'id', 'int', 11, 'auto_increment', 0, 19),
(86, 'userTypeName', 'varchar', 50, '', 0, 19),
(87, 'isActive', 'tinyint', 1, '', 0, 19),
(88, 'id', 'int', 11, 'auto_increment', 0, 20),
(89, 'userTypeId', 'int', 11, '', 0, 20),
(90, 'subActivityId', 'int', 11, '', 0, 20),
(91, 'isActive', 'tinyint', 1, '', 0, 20),
(92, 'creationDate', 'datetime', 0, '', 0, 20),
(93, 'id', 'INT', 10, 'AUTO_INCREMENT', 0, 22),
(94, 'divisionName', 'VARCHAR', 15, '', 0, 22),
(95, 'isActive', 'INT', 1, '', 0, 22),
(96, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 21),
(97, 'positionName', 'VARCHAR', 150, '', 0, 21),
(98, 'divisionId', 'INT', 15, '', 0, 21),
(99, 'positionOrder', 'INT', 15, '', 0, 21),
(100, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 23),
(101, 'title', 'VARCHAR', 150, '', 0, 23),
(102, 'firstName', 'VARCHAR', 150, '', 0, 23),
(103, 'lastName', 'VARCHAR', 150, '', 0, 23),
(104, 'midName', 'VARCHAR', 150, '', 0, 23),
(105, 'positionId', 'INT', 15, '', 0, 23),
(106, 'profilebio', ' TEXT', 0, '', 0, 23),
(107, 'id', 'INT', 0, 'PK', 0, 24),
(108, 'path', 'VARCHAR', 250, '', 0, 24),
(109, 'imgString', ' TEXT', 0, '', 0, 24),
(110, 'creationDate', 'DATETIME', 0, 'DEFAULT CURRENT_TIMESTAMP', 1, 24),
(111, 'isActive', 'INT', 1, '', 0, 24),
(112, 'id', 'INT', 0, 'AUTO_INCREMENT', 0, 25),
(113, 'errorMsg', 'VARCHAR', 250, '', 0, 25),
(114, 'urlAccess', 'VARCHAR', 250, '', 0, 25),
(115, 'remoteIP', 'VARCHAR', 250, '', 0, 25),
(116, 'creationTime', 'DATETIME', 0, 'DEFAULT CURRENT_TIMESTAMP', 0, 25),
(117, 'id', 'int', 15, '', 0, 26),
(118, 'title', 'varchar', 150, '', 0, 26),
(119, 'firstName', 'varchar', 150, '', 0, 26),
(120, 'lastName', 'varchar', 150, '', 0, 26),
(121, 'midName', 'varchar', 150, '', 0, 26),
(122, 'positionId', 'int', 15, '', 0, 26),
(123, 'profilebio', 'text', 0, '', 0, 26),
(124, 'positionName', 'varchar', 150, '', 0, 26),
(125, 'path', 'varchar', 250, '', 0, 26),
(126, 'divisionName', 'varchar', 150, '', 0, 26),
(127, 'divisionId', 'int', 15, '', 0, 26),
(128, 'positionOrder', 'int', 15, '', 0, 26),
(129, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 27),
(130, 'countryName', 'VARCHAR', 150, '', 0, 27),
(131, 'id', 'INT', 15, 'PK', 0, 28),
(132, 'court', 'VARCHAR', 150, '', 0, 28),
(133, 'address', 'VARCHAR', 150, '', 0, 28),
(134, 'district', 'VARCHAR', 150, '', 0, 28),
(135, 'country', 'INT', 15, '', 0, 28),
(136, 'parentId', 'INT', 15, '', 1, 28),
(137, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 29),
(138, 'town', 'VARCHAR', 150, '', 0, 29),
(139, 'tel', 'VARCHAR', 150, '', 0, 29),
(140, 'email', 'VARCHAR', 150, '', 0, 29),
(141, 'registryId', 'INT', 15, '', 0, 29),
(142, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 30),
(143, 'caseNo', 'VARCHAR', 150, '', 0, 30),
(144, 'applicant', 'VARCHAR', 250, '', 0, 30),
(145, 'respondent', 'VARCHAR', 250, '', 0, 30),
(146, 'filingDate', 'DATE', 0, '', 0, 30),
(147, 'caseHeadline', 'VARCHAR', 150, '', 0, 30),
(148, 'hearingDate', 'DATE', 0, '', 0, 30),
(149, 'status', 'VARCHAR', 15, '', 0, 30),
(150, 'cite', 'VARCHAR', 150, '', 0, 30),
(151, 'rulingDate', 'VARCHAR', 150, '', 0, 30),
(152, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 31),
(153, 'judgeId', 'INT', 15, '', 0, 31),
(154, 'caseId', 'INT', 15, '', 0, 31),
(155, 'isActive', 'INT', 1, '', 0, 31),
(156, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 32),
(157, 'content', ' TEXT', 0, '', 0, 32),
(158, 'creationDate', 'DATETIME', 0, 'DEFAULT CURRENT_TIMESTAMP', 1, 32),
(159, 'isActive', 'INT', 1, '', 0, 32),
(160, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 33),
(161, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 34),
(162, 'caseId', 'VARCHAR', 50, '', 0, 34),
(163, 'path', ' TEXT', 0, '', 0, 34),
(164, 'fileName', 'VARCHAR', 150, '', 0, 34),
(165, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 35),
(166, 'caseId', 'INT', 15, '', 0, 35),
(167, 'Date', 'DATE', 0, '', 0, 35),
(168, 'isActive', 'INT', 1, '', 0, 35),
(169, 'content', ' TEXT', 0, '', 0, 33),
(170, 'caseId', 'INT', 15, '', 0, 33),
(171, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 36),
(172, 'wordval', ' TEXT', 0, '', 0, 36),
(173, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 37),
(174, 'stageName', 'VARCHAR', 150, '', 0, 37),
(175, 'TrueNextStageId', 'INT', 15, '', 0, 37),
(176, 'FalseNextStageId', 'INT', 15, '', 0, 37),
(177, 'webForm', 'VARCHAR', 250, '', 0, 37),
(178, 'mobileUrl', 'VARCHAR', 250, '', 0, 37),
(179, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 38),
(180, 'documentName', 'VARCHAR', 150, '', 0, 38),
(181, 'documentDetail', 'VARCHAR', 250, '', 0, 38),
(183, 'id', 'INT', 15, '', 0, 39),
(184, 'path', 'VARCHAR', 250, '', 0, 39),
(185, 'hashVal', 'VARCHAR', 250, '', 0, 39),
(186, 'isActive', 'INT', 1, '', 0, 39),
(187, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 40),
(188, 'requirement', 'text', 0, '', 0, 40),
(189, 'amount', 'INT', 15, '', 0, 40),
(190, 'documentId', 'INT', 15, '', 0, 40),
(191, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 41),
(192, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 42),
(193, 'FirstName', 'VARCHAR', 150, '', 0, 42),
(194, 'lastName', 'VARCHAR', 150, '', 0, 42),
(195, 'MobileNumber', 'VARCHAR', 150, '', 0, 42),
(196, 'Email', 'VARCHAR', 50, '', 0, 42),
(197, 'Password', 'VARCHAR', 150, '', 0, 42),
(198, 'ConfirmPassword', 'VARCHAR', 150, '', 0, 42),
(199, 'Profession', 'VARCHAR', 10, '', 0, 42),
(200, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 43),
(201, 'answer', 'VARCHAR', 50, '', 0, 43),
(202, 'value', 'INT', 15, '', 0, 43),
(203, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 44),
(204, 'questionNo', 'INT', 15, '', 0, 44),
(205, 'questionWord', ' TEXT', 0, '', 0, 44),
(206, 'priorty', 'VARCHAR', 50, '', 0, 44),
(207, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 45),
(208, 'qnsId', 'INT', 15, '', 0, 45),
(209, 'answerId', 'INT', 15, '', 0, 45),
(210, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 46),
(211, 'qnsanswerId', 'INT', 15, '', 0, 46),
(212, 'userId', 'INT', 15, '', 0, 46),
(213, 'democaseId', 'INT', 15, '', 0, 46),
(214, 'creationDate', 'DATETIME', 0, 'DEFAULT CURRENT_TIMESTAMP', 1, 46),
(215, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 47),
(216, 'creationDate', 'DATETIME', 0, 'DEFAULT CURRENT_TIMESTAMP', 1, 47),
(217, 'userId', 'VARCHAR', 150, '', 0, 47),
(218, 'reviewed', 'INT', 1, '', 1, 47),
(219, 'reviewedBy', 'VARCHAR', 150, '', 0, 47),
(220, 'reviewDate', 'DATETIME', 0, 'ON UPDATE CURRENT_TIMESTAMP', 1, 47),
(221, 'comment', 'VARCHAR', 150, '', 0, 47),
(222, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 48),
(223, 'demoCaseId', 'INT', 15, '', 0, 48),
(224, 'qnsId', 'INT', 15, '', 0, 48),
(225, 'ansId', 'INT', 15, '', 0, 48),
(226, 'id', 'VARCHAR', 15, 'PK', 0, 49),
(227, 'weight', 'INT', 15, '', 0, 49),
(228, 'id', 'int', 11, 'auto_increment', 0, 50),
(229, 'statusName', 'varchar', 50, '', 0, 50),
(230, 'isActive', 'tinyint', 1, '', 0, 50),
(231, 'id', 'int', 11, 'auto_increment', 0, 51),
(232, 'userId', 'varchar', 50, '', 0, 51),
(233, 'code', 'varchar', 150, '', 0, 51),
(234, 'isActive', 'tinyint', 1, '', 0, 51),
(235, 'creationDate', 'datetime', 0, '', 0, 51),
(236, 'verificationDate', 'datetime', 0, 'on update current_timestamp()', 1, 51),
(237, 'demoCaseId', 'int', 15, '', 0, 52),
(238, 'productofsum', 'bigint', 29, '', 0, 52),
(239, 'itemNo', 'bigint', 21, '', 0, 52),
(240, 'factor', 'decimal', 29, '', 1, 52),
(241, 'demoCaseId', 'int', 15, '', 0, 53),
(242, 'productofsum', 'bigint', 29, '', 0, 53),
(243, 'itemNo', 'bigint', 21, '', 0, 53),
(244, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 54),
(245, 'documentId', 'INT', 15, '', 0, 54),
(246, 'procId', 'INT', 15, '', 0, 54),
(247, 'paymentMode', 'INT', 15, '', 0, 54),
(248, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 55),
(249, 'mode', 'VARCHAR', 50, '', 0, 55),
(250, 'cost', 'INT', 15, '', 0, 54),
(251, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 56),
(252, 'procName', 'VARCHAR', 150, '', 0, 56),
(253, 'procDetail', 'VARCHAR', 150, '', 0, 56),
(254, 'orderIndex', 'INT', 15, '', 0, 56),
(255, 'nextQnsId', 'INT', 15, '', 1, 45),
(256, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 57),
(257, 'demoCaseId', 'INT', 15, '', 0, 57),
(258, 'subject', 'VARCHAR', 250, '', 0, 57),
(259, 'msg', ' TEXT', 0, '', 0, 57),
(260, 'creationDate', 'DATETIME', 0, '', 0, 57),
(261, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 58),
(262, 'siteName', 'VARCHAR', 250, '', 0, 58),
(263, 'district', 'VARCHAR', 150, '', 0, 58),
(264, 'lat', 'VARCHAR', 150, '', 0, 58),
(265, 'lng', 'VARCHAR', 150, '', 0, 58),
(266, 'isActive', 'INT', 1, '', 0, 58),
(267, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 59),
(268, 'firstName', 'VARCHAR', 150, '', 0, 59),
(269, 'lastName', 'VARCHAR', 150, '', 0, 59),
(270, 'contact', 'VARCHAR', 150, '', 0, 59),
(271, 'siteId', 'INT', 15, '', 0, 59),
(272, 'isActive', 'INT', 1, '', 0, 59),
(273, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 60),
(274, 'firstName', 'VARCHAR', 150, '', 0, 60),
(275, 'lastName', 'VARCHAR', 150, '', 0, 60),
(276, 'mobileNumber', 'VARCHAR', 150, '', 0, 60),
(277, 'status', 'VARCHAR', 150, '', 0, 60),
(278, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 61),
(279, 'userId', 'VARCHAR', 15, '', 0, 61),
(280, 'pword', 'VARCHAR', 15, '', 0, 61),
(281, 'isActive', 'INT', 15, '', 0, 61),
(282, 'creationDate', 'DATETIME', 0, 'DEFAULT CURRENT_TIMESTAMP', 0, 61),
(283, 'moditionDate', 'DATETIME', 0, 'ON UPDATE CURRENT_TIMESTAMP', 0, 61),
(284, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 62),
(285, 'processName', 'VARCHAR', 150, '', 0, 62),
(286, 'processDescription', 'VARCHAR', 250, '', 0, 62),
(287, 'NextProcessId', 'INT', 15, '', 0, 62),
(288, 'processTypeId', 'INT', 15, '', 1, 62),
(289, 'entityId', 'INT', 15, '', 1, 62),
(290, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 63),
(291, 'airwaybillId', 'INT', 15, '', 0, 63),
(292, 'workflowId', 'INT', 15, '', 0, 63),
(293, 'createdBy', 'VARCHAR', 150, '', 0, 63),
(294, 'creationDate', 'DATETIME', 0, 'DEFAULT CURRENT_TIMESTAMP', 1, 63),
(295, 'completedBy', 'VARCHAR', 150, '', 1, 63),
(296, 'completionDate', 'DATETIME', 0, '', 1, 63),
(297, 'isActive', 'INT', 1, '', 0, 63),
(298, 'id', 'INT', 15, 'PK', 0, 64),
(299, 'origin', 'VARCHAR', 250, '', 0, 64),
(300, 'captureDate', 'DATE', 0, '', 0, 64),
(301, 'destination', 'VARCHAR', 250, '', 0, 64),
(302, 'contents', 'VARCHAR', 250, '', 0, 64),
(303, 'customsValue', 'VARCHAR', 250, '', 1, 64),
(304, 'insuranceValue', 'VARCHAR', 250, '', 1, 64),
(305, 'dimension', 'VARCHAR', 150, '', 1, 64),
(306, 'noOfPieces', 'INT', 15, '', 0, 64),
(307, 'volWeight', 'FLOAT', 15, '', 1, 64),
(308, 'actualWeight', 'FLOAT', 15, '', 1, 64),
(309, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 65),
(310, 'name', 'VARCHAR', 150, '', 0, 65),
(311, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 66),
(312, 'airwaybillId', 'INT', 15, '', 0, 66),
(313, 'airwaybillDetailTypeId', 'VARCHAR', 150, '', 0, 66),
(314, 'accountNumber', 'VARCHAR', 150, '', 0, 66),
(315, 'referenceName', 'VARCHAR', 150, '', 1, 66),
(316, 'name', 'VARCHAR', 150, '', 0, 66),
(317, 'phoneNumber', 'VARCHAR', 150, '', 0, 66),
(318, 'companyName', 'VARCHAR', 150, '', 0, 66),
(319, 'streetAddress', 'VARCHAR', 150, '', 0, 66),
(320, 'city', 'VARCHAR', 150, '', 0, 66),
(321, 'postalCode', 'VARCHAR', 150, '', 1, 66),
(322, 'country', 'VARCHAR', 150, '', 1, 66),
(323, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 67),
(324, 'airwayBillId', 'INT', 15, '', 0, 67),
(325, 'instruction', 'VARCHAR', 250, '', 0, 67),
(326, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 68),
(327, 'templateName', 'VARCHAR', 150, '', 0, 68),
(328, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 69),
(329, 'airwaybillId', 'INT', 15, '', 0, 69),
(330, 'airwaybillTemplateId', 'INT', 15, '', 0, 69),
(331, 'amount', 'FLOAT', 15, '', 0, 69),
(332, 'id', 'VARCHAR', 13, 'PK', 0, 70),
(333, 'company', 'VARCHAR', 150, '', 0, 70),
(334, 'address', 'VARCHAR', 150, '', 0, 70),
(335, 'phoneNumber', 'VARCHAR', 150, '', 0, 70),
(336, 'city', 'VARCHAR', 150, '', 0, 70),
(337, 'id', 'INT', 15, 'AUTO_INCREMENT', 0, 71),
(338, 'billId', 'VARCHAR', 150, '', 0, 71),
(339, 'driverId', 'INT', 15, '', 0, 71),
(340, 'isActive', 'INT', 1, '', 0, 71),
(341, 'comment', 'VARCHAR', 150, '', 0, 71),
(342, 'modificationDate', 'DATETIME', 0, '', 0, 71);

-- --------------------------------------------------------

--
-- Table structure for table `entityform`
--

CREATE TABLE `entityform` (
  `colName` varchar(150) NOT NULL,
  `controlName` varchar(150) DEFAULT NULL,
  `labelName` varchar(50) NOT NULL,
  `url` varchar(250) DEFAULT NULL,
  `txtVal` varchar(150) DEFAULT NULL,
  `valval` varchar(150) DEFAULT NULL,
  `isActive` int(11) DEFAULT NULL,
  `tableNme` varchar(150) NOT NULL,
  `orderIn` int(11) DEFAULT NULL,
  `positionIn` int(11) DEFAULT NULL,
  `isRequired` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entityform`
--

INSERT INTO `entityform` (`colName`, `controlName`, `labelName`, `url`, `txtVal`, `valval`, `isActive`, `tableNme`, `orderIn`, `positionIn`, `isRequired`) VALUES
('billId', 'textbox', 'Bill Id', '', '', '', 0, 'airwaybilldriver', 1, NULL, 1),
('comment', 'textarea', 'Comment', '', '', '', 1, 'airwaybilldriver', 4, NULL, 1),
('driverId', 'combobox', 'Driver', '../mobileusers/viewcombobox', 'name', 'id', 1, 'airwaybilldriver', 2, NULL, 1),
('id', 'textbox', 'Id', '', '', '', 0, 'airwaybilldriver', 0, NULL, 1),
('isActive', 'textbox', 'Is Active', '', '', '', 0, 'airwaybilldriver', 3, NULL, 1),
('modificationDate', 'textbox', 'Modification Date', '', '', '', 0, 'airwaybilldriver', 5, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `errorhandler`
--

CREATE TABLE `errorhandler` (
  `id` int(11) NOT NULL,
  `errorMsg` varchar(250) NOT NULL,
  `urlAccess` varchar(250) NOT NULL,
  `remoteIP` varchar(250) NOT NULL,
  `creationTime` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `errorhandler`
--

INSERT INTO `errorhandler` (`id`, `errorMsg`, `urlAccess`, `remoteIP`, `creationTime`) VALUES
(1, 'pageNotfound', '', '127.0.0.1', '2021-04-18 14:20:52'),
(2, 'pageNotfound', 'http://localhost/justicehive/public/errorhandlers/error/pageNotfound', '127.0.0.1', '2021-04-18 14:24:36'),
(3, 'pageNotfound', 'http://localhost/justicehive/errorhandlers/error/pageNotfound', '127.0.0.1', '2021-04-18 14:24:50'),
(4, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-04-18 14:26:23'),
(5, 'PageNotFound', 'http://localhost/justicehive/public/errorhandlers/error/pageNotfound', '127.0.0.1', '2021-04-18 15:38:53'),
(6, 'PageNotFound', 'http://localhost/justicehive/public/errorhandlers/error/pageNotfound', '::1', '2021-04-18 16:05:41'),
(7, 'PageNotFound', 'http://localhost/justicehive/public/errorhandlers/error/pageNotfound', '::1', '2021-04-18 19:46:37'),
(8, 'PageNotFound', 'http://localhost/justicehive/public/errorhandlers/error/pageNotfound', '::1', '2021-04-19 02:38:30'),
(9, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error/pageNotfound', '::1', '2021-04-19 02:45:39'),
(10, 'PageNotFound', 'http://localhost/justicehive/public/errorhandlers/error/pageNotfound', '127.0.0.1', '2021-04-19 04:46:54'),
(11, 'PageNotFound', 'http://localhost/justicehive/public/errorhandlers/error/pageNotfound', '127.0.0.1', '2021-04-19 10:05:01'),
(12, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error/pageNotfound', '127.0.0.1', '2021-04-19 10:10:20'),
(13, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error/', '127.0.0.1', '2021-04-19 10:10:31'),
(14, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-04-19 10:10:37'),
(15, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 02:41:14'),
(16, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 02:44:53'),
(17, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 02:48:46'),
(18, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 02:52:45'),
(19, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 02:54:46'),
(20, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 02:56:58'),
(21, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 02:57:51'),
(22, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:01:37'),
(23, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:05:37'),
(24, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:07:16'),
(25, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:20:10'),
(26, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:21:05'),
(27, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:21:34'),
(28, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:22:16'),
(29, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:22:35'),
(30, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:24:09'),
(31, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:24:41'),
(32, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:25:59'),
(33, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:26:55'),
(34, 'PageNotFound', 'http://localhost/justicehive/errorhandlers/error', '127.0.0.1', '2021-05-04 03:27:31');

-- --------------------------------------------------------

--
-- Table structure for table `guitoolbox`
--

CREATE TABLE `guitoolbox` (
  `id` int(15) NOT NULL,
  `guiToolName` varchar(150) NOT NULL,
  `openTag` varchar(250) NOT NULL,
  `closeTag` varchar(250) DEFAULT NULL,
  `dispayHtml` varchar(250) DEFAULT NULL,
  `defaultClass` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guitoolbox`
--

INSERT INTO `guitoolbox` (`id`, `guiToolName`, `openTag`, `closeTag`, `dispayHtml`, `defaultClass`) VALUES
(1, 'Layout Grid', 'PGRpdj4=', 'PC9kaXY+', 'PGkgY2xhc3M9J2ZhIGZhLWJhcnMnPjwvaT4=', ''),
(2, 'Paragraphy', 'PHA+', 'PC9wPg==', 'PGkgY2xhc3M9J2ZhIGZhLXBhcmFncmFwaCc+PC9pPg==', ''),
(3, 'Heading 1', 'PGgxPg==', 'PC9oMT4=', 'PGkgY2xhc3M9J2ZhIGZhLWhlYWRlcic+IDE8L2k+', ''),
(4, 'Heading 2', 'PGgyPg==', 'PC9oMj4=', 'PGkgY2xhc3M9J2ZhIGZhLWhlYWRlcic+IDI8L2k+', ''),
(5, 'Heading 3', 'PGgzPg==', 'PC9oMz4=', 'PGkgY2xhc3M9J2ZhIGZhLWhlYWRlcic+IDM8L2k+', ''),
(6, 'Heading 4', 'PGg0Pg==', 'PC9oND4=', 'PGkgY2xhc3M9J2ZhIGZhLWhlYWRlcic+IDQ8L2k+', ''),
(7, 'heading 5', 'PGg1Pg==', 'PC9oNT4=', 'PGkgY2xhc3M9J2ZhIGZhLWhlYWRlcic+IDU8L2k+', ''),
(8, 'heading 6', 'PGg2Pg==', 'PC9oNj4=', 'PGkgY2xhc3M9J2ZhIGZhLWhlYWRlcic+ICA2PC9pPg==', ''),
(9, 'Link', 'PGE+', 'PC9hPg==', 'PGkgY2xhc3M9J2ZhIGZhLWxpbmsnPjwvaT4=', ''),
(10, 'Image', 'PGltZz4=', 'PC9pbWc+', 'PGkgY2xhc3M9J2ZhIGZhLWltYWdlJz48L2k+', ''),
(11, 'Un order List', 'PHVsPg==', 'PC91bD4=', 'PGkgY2xhc3M9J2ZhIGZhLWxpc3QtdWwnPiA8L2k+', ''),
(12, 'Order List', 'PG9sPg==', 'PC9vbD4=', 'PGkgY2xhc3M9J2ZhIGZhLWxpc3Qtb2wnPjwvaT4=', ''),
(13, 'List Item', 'PGxpPg==', 'PC9saT4=', 'PGkgY2xhc3M9J2ZhIGZhLWRvdC1jaXJjbGUtbyc+IDwvaT4=', ''),
(14, 'Table', 'PHRhYmxlPg==', 'PC90YWJsZT4=', 'PGkgY2xhc3M9J2ZhIGZhLXRhYmxlJz48L2k+', ''),
(15, 'Table Row', 'PHRyPg==', 'PC90cj4=', 'PGkgY2xhc3M9J2ZhIGZhLXRoLWxhcmdlJz4gPC9pPg==', ''),
(16, 'Table Header', 'PHRoPg==', 'PC90aD4=', 'PGkgY2xhc3M9J2ZhIGZhLXRoJz48L2k+', ''),
(17, 'table Data', 'PHRkPg==', 'PC90ZD4=', 'PGkgY2xhc3M9J2ZhIGZhLXNxdWFyZSc+PC9pPg==', '');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `judge`
--

CREATE TABLE `judge` (
  `id` int(15) NOT NULL,
  `title` varchar(150) NOT NULL,
  `firstName` varchar(150) NOT NULL,
  `lastName` varchar(150) NOT NULL,
  `midName` varchar(150) NOT NULL,
  `positionId` int(15) NOT NULL,
  `profilebio` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `judge`
--

INSERT INTO `judge` (`id`, `title`, `firstName`, `lastName`, `midName`, `positionId`, `profilebio`) VALUES
(1, 'Hon', 'Yiga', 'James', 'ss', 3, 'ssssssss');

-- --------------------------------------------------------

--
-- Table structure for table `judgepge`
--

CREATE TABLE `judgepge` (
  `id` int(11) NOT NULL,
  `path` varchar(250) NOT NULL,
  `imgString` text NOT NULL,
  `creationDate` datetime DEFAULT current_timestamp(),
  `isActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `judgepge`
--

INSERT INTO `judgepge` (`id`, `path`, `imgString`, `creationDate`, `isActive`) VALUES
(1, '../public/import/images/face1.jpg', '', '2021-04-20 00:19:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `judgeposition`
--

CREATE TABLE `judgeposition` (
  `id` int(15) NOT NULL,
  `positionName` varchar(150) NOT NULL,
  `divisionId` int(15) NOT NULL,
  `positionOrder` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `judgeposition`
--

INSERT INTO `judgeposition` (`id`, `positionName`, `divisionId`, `positionOrder`) VALUES
(1, 'President of the Court', 1, 1),
(2, 'Vice-Presdent of the Court', 1, 2),
(3, 'Judge', 1, 3),
(4, 'Principal Judge', 2, 1),
(5, 'Deputy Principal Judge', 2, 2),
(6, 'Judge', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `listhandle`
--

CREATE TABLE `listhandle` (
  `id` int(11) NOT NULL,
  `idVal` varchar(150) NOT NULL,
  `textVal` varchar(150) NOT NULL,
  `listName` varchar(150) NOT NULL,
  `isSync` int(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listhandle`
--

INSERT INTO `listhandle` (`id`, `idVal`, `textVal`, `listName`, `isSync`) VALUES
(1, '1', 'YES', 'isActive', 0),
(2, '0', 'No', 'isActive', 0),
(3, 'Female', 'Female', 'SEX', 0),
(4, 'Male', 'Male', 'SEX', 0),
(5, 'Single', 'Single', 'martialStatus', 0),
(6, 'Married', 'Married', 'martialStatus', 0),
(7, 'Employed', 'Employed', 'employmentStatus', 0),
(8, 'Unemployed', 'Unemployed', 'employmentStatus', 0),
(9, 'Self Employed', 'Self Employed', 'employmentStatus', 0),
(10, 'INT', 'INT', 'dataType', 0),
(11, 'DECIMAL', 'DECIMAL', 'dataType', 0),
(12, 'FLOAT', 'FLOAT', 'dataType', 0),
(13, 'DOUBLE', 'DOUBLE', 'dataType', 0),
(14, 'CHAR', 'CHAR', 'dataType', 0),
(15, 'VARCHAR', 'VARCHAR', 'dataType', 0),
(16, ' TEXT', ' TEXT', 'dataType', 0),
(17, 'DATE', 'DATE', 'dataType', 0),
(18, 'TIME', 'TIME', 'dataType', 0),
(19, 'DATETIME', 'DATETIME', 'dataType', 0),
(20, 'JSON', 'JSON', 'dataType', 0),
(21, 'AUTO_INCREMENT', 'AUTO_INCREMENT', 'DB_CONSTRAINT', 0),
(22, 'PK', 'PK', 'DB_CONSTRAINT', 0),
(23, 'css', 'css', 'fileType', 0),
(24, 'js', 'js', 'fileType', 0),
(25, 'ON UPDATE CURRENT_TIMESTAMP', 'ON UPDATE CURRENT_TIMESTAMP', 'DB_CONSTRAINT', 0),
(26, 'DEFAULT CURRENT_TIMESTAMP', 'DEFAULT CURRENT_TIMESTAMP', 'DB_CONSTRAINT', 0),
(27, 'Piece', 'Piece', 'uom', 0),
(28, 'Kgs', 'Kgs', 'uom', 0),
(29, 'Drozen', 'Drozen', 'uom', 0),
(30, 'Carton', 'Carton', 'uom', 0),
(31, 'slide', 'slide', 'resType', 0),
(32, 'image', 'image', 'resType', 0),
(35, 'id', 'categoryName', 'Category', 0),
(37, 'id', 'businessname', 'businessprofile', 1),
(38, 'id', 'action', 'routineactive', 1),
(39, 'id', 'operationName', 'operationlist', 1),
(40, 'id', 'intervalValue', 'serviceinterval', 1),
(41, 'id', 'processNameType', 'processtype', 1),
(43, 'id', 'documentName', 'document', 1),
(44, 'file', 'file', 'dataType', 0),
(45, 'id', 'stageName', 'stage', 1),
(46, 'maker', 'maker', 'stageType', 0),
(47, 'checker', 'checker', 'stageType', 0),
(48, 'System Service', 'System Service', 'stageType', 0),
(49, '1', 'Approved', 'approvalStatus', 0),
(50, '0', 'Declined', 'approvalStatus', 0),
(51, 'id', 'divisionName', 'eacjdivison', 1),
(52, 'id', 'positionName', 'judgeposition', 1),
(53, 'id', 'countryName', 'country', 1),
(54, 'Lawyer', 'Lawyer', 'profession', 0),
(55, 'Judge', 'Judge', 'profession', 0),
(56, 'Others', 'Others', 'profession', 0),
(57, 'high', 'HIGH', 'Priorty', 0),
(58, 'Moderate', 'MODERATE', 'Priorty', 0),
(59, 'low', 'LOW', 'Priorty', 0),
(60, 'id', 'answer', 'checklistanswer', 1),
(61, 'id', 'requirement', 'applyrequirement', 1),
(62, 'id', 'mode', 'documentaccessmode', 1),
(63, 'id', 'processName', 'process', 1),
(64, 'id', 'firstName', 'mobileuser', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mauthentication`
--

CREATE TABLE `mauthentication` (
  `id` int(15) NOT NULL,
  `userId` varchar(250) NOT NULL,
  `pword` varchar(250) NOT NULL,
  `isActive` int(15) NOT NULL,
  `creationDate` datetime DEFAULT current_timestamp(),
  `modificationDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mobileuser`
--

CREATE TABLE `mobileuser` (
  `id` varchar(150) NOT NULL,
  `firstName` varchar(150) NOT NULL,
  `lastName` varchar(150) NOT NULL,
  `mobileNumber` varchar(150) NOT NULL,
  `status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mobileuser`
--

INSERT INTO `mobileuser` (`id`, `firstName`, `lastName`, `mobileNumber`, `status`) VALUES
('{81C65E28-F021-6319-CBE6-EDA4090FB9EA}', 'James', 'Yiga', '0781587081', 'Verify');

-- --------------------------------------------------------

--
-- Table structure for table `panel`
--

CREATE TABLE `panel` (
  `id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `proc`
--

CREATE TABLE `proc` (
  `id` int(15) NOT NULL,
  `procName` varchar(150) NOT NULL,
  `procDetail` varchar(150) NOT NULL,
  `orderIndex` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `proc`
--

INSERT INTO `proc` (`id`, `procName`, `procDetail`, `orderIndex`) VALUES
(1, 'Citation and Commence', 'Citation and Commence', 1),
(2, 'Stage 2', 'This is the second stage', 2),
(3, 'Stage 3', 'Stage 3', 3);

-- --------------------------------------------------------

--
-- Table structure for table `procdoc`
--

CREATE TABLE `procdoc` (
  `id` int(15) NOT NULL,
  `documentId` int(15) NOT NULL,
  `procId` int(15) NOT NULL,
  `paymentMode` int(15) NOT NULL,
  `amount` int(15) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `procdoc`
--

INSERT INTO `procdoc` (`id`, `documentId`, `procId`, `paymentMode`, `amount`) VALUES
(1, 1, 0, 1, 0),
(2, 1, 0, 1, 0),
(3, 1, 1, 1, 0),
(4, 0, 2, 0, 0),
(5, 0, 3, 0, 0),
(6, 0, 3, 0, 500),
(7, 3, 3, 3, 500);

-- --------------------------------------------------------

--
-- Table structure for table `process`
--

CREATE TABLE `process` (
  `id` int(15) NOT NULL,
  `processName` varchar(150) NOT NULL,
  `processDescription` varchar(250) NOT NULL,
  `NextProcessId` int(15) NOT NULL,
  `processTypeId` int(15) DEFAULT NULL,
  `entityId` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `questionanswer`
--

CREATE TABLE `questionanswer` (
  `id` int(15) NOT NULL,
  `qnsId` int(15) NOT NULL,
  `answerId` int(15) NOT NULL,
  `nextQnsId` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questionanswer`
--

INSERT INTO `questionanswer` (`id`, `qnsId`, `answerId`, `nextQnsId`) VALUES
(1, 1, 1, 2),
(6, 1, 2, 2),
(8, 2, 2, 3),
(9, 2, 1, 4),
(10, 3, 2, 4),
(11, 3, 1, 4),
(12, 3, 3, 4),
(13, 4, 2, 5),
(14, 4, 1, 5),
(15, 4, 3, 6),
(16, 5, 2, 6),
(17, 5, 1, 6),
(18, 5, 3, 6),
(19, 5, 4, 6),
(20, 6, 2, NULL),
(21, 6, 1, NULL),
(22, 6, 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rankingval`
--

CREATE TABLE `rankingval` (
  `id` varchar(15) NOT NULL,
  `weight` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rankingval`
--

INSERT INTO `rankingval` (`id`, `weight`) VALUES
('High', 10),
('Less', 2),
('Low', 1),
('Middle', 5);

-- --------------------------------------------------------

--
-- Table structure for table `registry`
--

CREATE TABLE `registry` (
  `id` int(15) NOT NULL,
  `court` varchar(150) NOT NULL,
  `address` varchar(150) NOT NULL,
  `district` varchar(150) NOT NULL,
  `country` int(15) NOT NULL,
  `parentId` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registry`
--

INSERT INTO `registry` (`id`, `court`, `address`, `district`, `country`, `parentId`) VALUES
(1, 'ffff', 'fff2', 'fff', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rolemanagement`
--

CREATE TABLE `rolemanagement` (
  `id` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  `userId` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rolemanagement`
--

INSERT INTO `rolemanagement` (`id`, `roleId`, `userId`) VALUES
(1, 3, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(284, 4, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(299, 14, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(300, 2, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(307, 7, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(309, 92, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(328, 93, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(341, 1, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(360, 112, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(362, 114, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(363, 115, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(366, 118, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(367, 119, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(397, 145, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(398, 146, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(399, 147, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(400, 148, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(401, 149, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(402, 150, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(403, 151, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(404, 152, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(405, 153, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(406, 155, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(407, 158, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(408, 159, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(411, 163, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(412, 166, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(413, 168, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(414, 170, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(417, 173, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(419, 175, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(420, 177, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(421, 178, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(422, 182, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(423, 184, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(424, 185, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}'),
(425, 186, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}');

-- --------------------------------------------------------

--
-- Table structure for table `searchword`
--

CREATE TABLE `searchword` (
  `id` int(15) NOT NULL,
  `wordval` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `searchword`
--

INSERT INTO `searchword` (`id`, `wordval`) VALUES
(1, 'trade'),
(2, 'payment'),
(3, 'not'),
(4, 'Commodity');

-- --------------------------------------------------------

--
-- Table structure for table `shipperdetail`
--

CREATE TABLE `shipperdetail` (
  `id` int(15) NOT NULL,
  `firstName` varchar(150) NOT NULL,
  `lastName` varchar(150) NOT NULL,
  `contact` varchar(150) NOT NULL,
  `siteId` int(15) NOT NULL,
  `isActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shipperdetail`
--

INSERT INTO `shipperdetail` (`id`, `firstName`, `lastName`, `contact`, `siteId`, `isActive`) VALUES
(1, 'James', 'Yiga', '0781587081', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE `signup` (
  `id` int(15) NOT NULL,
  `FirstName` varchar(150) NOT NULL,
  `lastName` varchar(150) NOT NULL,
  `MobileNumber` varchar(150) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(150) NOT NULL,
  `ConfirmPassword` varchar(150) NOT NULL,
  `Profession` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `stage`
--

CREATE TABLE `stage` (
  `id` int(15) NOT NULL,
  `stageName` varchar(150) NOT NULL,
  `TrueNextStageId` int(15) NOT NULL,
  `FalseNextStageId` int(15) NOT NULL,
  `webForm` varchar(250) NOT NULL,
  `mobileUrl` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stage`
--

INSERT INTO `stage` (`id`, `stageName`, `TrueNextStageId`, `FalseNextStageId`, `webForm`, `mobileUrl`) VALUES
(1, 'Create Case', 2, 0, '../democase/create', 'createcase'),
(2, 'Get Started', 4, 3, '../docuement/list', 'documentlist'),
(3, 'Hire Anatomy', 0, 0, '../xxx/xxx', 'xxxx'),
(4, 'File Case', 5, 0, '../democase/filecase', 'filecase'),
(5, 'Followup', 0, 0, '../democase/followup', 'followup');

-- --------------------------------------------------------

--
-- Table structure for table `statu`
--

CREATE TABLE `statu` (
  `id` int(11) NOT NULL,
  `statusName` varchar(50) NOT NULL,
  `isActive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statu`
--

INSERT INTO `statu` (`id`, `statusName`, `isActive`) VALUES
(1, 'verify', 1),
(2, 'verified', 1),
(4, 'deactivated', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subactivity`
--

CREATE TABLE `subactivity` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `link` varchar(225) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `activityId` int(11) NOT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `orderIndex` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subactivity`
--

INSERT INTO `subactivity` (`id`, `name`, `link`, `icon`, `activityId`, `isActive`, `orderIndex`) VALUES
(1, 'Modules', '../activitys/view', NULL, 5, 1, 1),
(2, 'Functional Spec', '../subActivitys/view', NULL, 5, 1, 2),
(3, 'Administration User', '../systemUsers/view', 'fa fa-users', 5, 1, 10),
(4, 'Role Management', '../roleManagements/view', NULL, 5, 1, 4),
(7, 'Code Builder', '../codeBuilders/view', NULL, 5, 1, 5),
(14, 'Setting System Value', '../configurationsettings/view', NULL, 5, 1, 7),
(92, 'Report Desiger', '../reports/view', 'fa fa-file-o', 5, 1, 11),
(93, 'Create List', '../listhandles/view', 'fa fa-list', 5, 1, 12),
(112, 'Manage Entity', '../entitys/view', 'fa fa-table', 24, 1, 1),
(114, 'Add Attrubite Keys', '../attkeys/view', 'fa fa-list', 5, 1, 10),
(115, 'Gui Tool Box Setup', '../guitoolboxs/view', 'fa fa-cogs', 5, 1, 12),
(118, 'Tool Box', '../toolboxs/view', 'fa fa-cog', 5, 1, 12),
(119, 'Theme', '../themes/view', 'fa fa-bookmark', 5, 1, 13),
(132, 'User Types Management', '../usertypes/view', 'fa fa-user', 28, 1, 1),
(133, 'Mobile Users', '../users/view', 'fa fa-user', 28, 1, 2),
(145, 'Manage Judge Positions', '../judgepositions/view', 'fa fa-file', 35, 1, 1),
(146, 'Manage Division', '../eacjdivisons/view', 'fa fa-file', 35, 1, 2),
(147, 'Manage Judges', '../judges/view', 'fa fa-user', 35, 1, 3),
(148, 'Set Member Country', '../countrys/view', 'fa fa-file', 35, 1, 4),
(149, 'Member Registry', '../registrys/view', 'fa fa-bank', 35, 1, 5),
(150, 'Cases', '../casenumbers/view', 'fa fa-file', 35, 1, 6),
(151, 'Manage Search Word Hint', '../searchwords/view', 'fa fa-file', 36, 1, 2),
(152, 'Manage Workflow', '../stages/view', 'fa fa-file', 36, 1, 2),
(153, 'Manage Document', '../documents/view', 'fa fa-file', 36, 1, 3),
(154, 'Resource centre', '../documentpges/view', 'fa fa-file', 36, 1, 4),
(155, 'Manage Application Requirement', '../applyrequirements/view', 'fa fa-file', 36, 1, 6),
(156, 'Manage panel', '../panels/view', 'fa fa-file', 5, 1, 15),
(157, 'Manage signup', '../signups/view', 'fa fa-file', 35, 1, 7),
(158, 'Manage checklist Answers', '../checklistanswers/view', 'fa fa-file', 36, 1, 6),
(159, 'Manage checklist', '../checklists/view', 'fa fa-file', 36, 1, 7),
(160, 'Manage questionanswer', '../questionanswers/view', 'fa fa-file', 37, 1, 1),
(161, 'Manage Applicants', '../democases/view', 'fa fa-file', 36, 1, 8),
(162, 'Applicant Answer', '../democaseanswers/view', 'fa fa-file', 37, 1, 2),
(163, 'Weight Point', '../rankingvals/view', 'fa fa-list', 36, 1, 9),
(164, 'Weight Point', '../rankingvals/view', 'fa fa-file', 36, 1, 10),
(165, 'Manage vwfactor', '../vwfactors/view', 'fa fa-file', 37, 1, 3),
(166, 'Manage Access Mode', '../documentaccessmodes/view', 'fa fa-file', 36, 1, 11),
(167, 'Manage procdoc', '../procdocs/view', 'fa fa-file', 37, 1, 4),
(168, 'Manage Procedure', '../procs/view', 'fa fa-file', 36, 1, 13),
(169, 'Manage procdoc', '../procdocs/view', 'fa fa-file', 37, 1, 5),
(170, 'Require Legal Assistance', '../democasecontacts/view', 'fa fa-file', 36, 1, 13),
(171, 'Manage collection Site', '../collectionsites/view', 'fa fa-file', 38, 1, 2),
(172, 'Manage shipperdetail', '../shipperdetails/view', 'fa fa-file', 37, 1, 6),
(173, 'Manage Driver', '../mobileusers/view', 'fa fa-file', 38, 1, 2),
(174, 'Manage Process', '../processs/view', 'fa fa-file', 5, 1, 14),
(175, 'Manage Process Flow', '../workflows/view', 'fa fa-file', 5, 1, 15),
(176, 'Manage workflowhistory', '../workflowhistorys/view', 'fa fa-file', 37, 1, 7),
(177, 'Create an Airway Bill', '../airwaybills/createnew', 'fa fa-file', 39, 1, 2),
(178, 'Manage airwaybilldetailtype', '../airwaybilldetailtypes/view', 'fa fa-file', 5, 1, 16),
(179, 'Manage airway bill detail', '../airwaybilldetails/view', 'fa fa-file', 37, 1, 8),
(180, 'Manage airway bill detail', '../airwaybilldetails/view', 'fa fa-file', 37, 1, 9),
(181, 'Manage airwaybillinstruction', '../airwaybillinstructions/view', 'fa fa-file', 37, 1, 10),
(182, 'Manage Charge List', '../airwaybillchargetemplates/view', 'fa fa-file', 38, 1, 3),
(183, 'Manage airwaybillcharge', '../airwaybillcharges/view', 'fa fa-file', 37, 1, 11),
(184, 'Manage Client Accounts', '../consigeeaccounts/view', 'fa fa-file', 38, 1, 4),
(185, 'Process Status', '../airwaybills/processStatus', 'fa fa-file', 39, 1, 1),
(186, 'Process an Airway Bill', '../airwaybills/processairwaybill', 'fa fa-file', 39, 1, 3),
(187, 'Manage Airway bill Driver', '../airwaybilldrivers/view', 'fa fa-file', 37, 1, 12);

-- --------------------------------------------------------

--
-- Table structure for table `systemuser`
--

CREATE TABLE `systemuser` (
  `id` varchar(250) NOT NULL,
  `firstName` varchar(200) NOT NULL,
  `lastName` varchar(200) NOT NULL,
  `dob` date NOT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `systemuser`
--

INSERT INTO `systemuser` (`id`, `firstName`, `lastName`, `dob`, `contact`, `email`, `isActive`, `username`, `password`) VALUES
('046e34ff-c15b-437c-9d0e-3781d975a6a8', 'Gril', 'Joe', '0000-00-00', '0781587081', 'Kkk@tt.com', 1, '0781587081', 'aSimkE77'),
('0d346480-820e-47c2-ac0c-e865b8bb49ab', 'Trysts', 'b', '0000-00-00', '0781587081', 'Jnsnsn@gmail.com', 1, '0781587081', 'aSimkE77'),
('34232a46-ed41-42de-a744-35f6a5928394', 'Teetered', 'bdbdb', '0000-00-00', '0781587081', 'james2yiga@gmail.com', 1, '0781587081', 'aSimkE77'),
('368f317c-b45d-42bc-a88c-98b21e471dda', 'Test', 'Test', '0000-00-00', '0781587081', 'Test@code360ds.com', 1, '0781587081', 'aSimkE77'),
('3827c646-5834-46fa-89d0-132e42364fa8', 'Test', 'Test', '0000-00-00', '0781587081', 'Test@code360ds.com', 1, '0781587081', 'aSimkE77'),
('38418485-5552-4290-a196-b67beef99b03', 'Peace', 'Nakyeyune', '0000-00-00', '0781587081', 'james2yiga@gmail.com', 1, '0781587081', 'KW3w1g+0VNnLPA=='),
('3b40b098-a817-4c92-85cb-f7b75664f154', 'Yeh', 'bdbbd', '0000-00-00', '0781587081', 'Jajaj@gmail.com', 1, '0781587081', 'aSimkE77'),
('5e233548-f283-45e9-8035-5b8531c55a09', 'Test', 'tyysb', '0000-00-00', '0781587081', 'Testing@cod.com', 1, '0781587081', 'aSimkE77'),
('603613cf-b6a4-4428-9880-dc14e6e7f784', 'Test', 'tyysb', '0000-00-00', '0781587081', 'Testing@cod.com', 1, '0781587081', 'aSimkE77'),
('61b14151-1978-4750-99c9-8a1fb20c2083', 'Teeth', 'ghdhd', '0000-00-00', '0781587081', 'kkkh@gmail.com', 1, '0781587081', 'aSimkE77'),
('693587ee-9c84-423c-a6ff-1cebe7009132', 'Yehdgsg', 'hgdgd', '0000-00-00', '0781587081', 'Jansns@gmail.com', 1, '0781587081', 'aSimkE77'),
('78f61d56-3c44-416a-8dfb-60db8803a1d6', 'Gggg', 'bbb', '0000-00-00', '0781587081', 'Jj@gmail.com', 1, '0781587081', 'aSimkE77'),
('91ad2716-30a8-4056-b936-a270ee2b95a1', 'Tesysy', 'yiga', '0000-00-00', '0781587081', 'Testing@info.com', 1, '0781587081', 'aSimkE77'),
('adff179d-66a0-420e-886f-4b33e915f03f', 'Send', 'db', '0000-00-00', '0781587081', 'Ken@gmail.com', 1, '0781587081', 'aSimkE77Fog='),
('aeb4d933-a14d-4fe8-8e52-31fa93d6d9cf', 'Test', 'ghh', '0000-00-00', '0782587081', 'Ops@code360ds.com', 1, '0782587081', 'aSimkE77'),
('bf08ccaa-7363-4a8a-bc8a-d2a1f5d8e617', 'Gaffs', 'hdbdb', '0000-00-00', '0781587081', 'hhh@xonib.com', 1, '0781587081', 'aSimkE77'),
('bf611045-b4ec-4187-a07a-17c0add67ef4', 'Gsgsgs', 'hdbshd', '0000-00-00', '0781587081', 'Jsjsjsj@xonib.com', 1, '0781587081', 'aSimkE77'),
('da0cf05d-27ad-4b9a-9121-54e735c76609', 'Tetegsgd', 'bbdbd', '0000-00-00', '0781587081', 'Hshsgs@hotdick.com', 1, '0781587081', 'aSimkE77'),
('{67EBA0E9-6F38-0F8E-644B-B07B6B5C73D9}', 'test', 'test3', '0000-00-00', '0781587081', 'james2yiga@gmail.com', 1, 'james2yiga@gmail.com', 'aSimkE77Foid'),
('{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}', 'James', 'Yiga', '1990-07-25', '+256781587081', 'james.yiga@sybrin.com', 1, 'jyiga', 'aSimkA==');

-- --------------------------------------------------------

--
-- Table structure for table `toolbox`
--

CREATE TABLE `toolbox` (
  `id` int(11) NOT NULL,
  `toolboxName` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toolbox`
--

INSERT INTO `toolbox` (`id`, `toolboxName`) VALUES
(1, 'textbox'),
(2, 'passwordbox'),
(3, 'combobox'),
(4, 'checkbox'),
(5, 'radiobutton'),
(6, 'dateboxPicker'),
(7, 'filePicker'),
(8, 'textarea'),
(9, 'button'),
(10, 'EditableTable');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(50) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `mobileNumber` varchar(20) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `creationDate` datetime NOT NULL DEFAULT current_timestamp(),
  `statusId` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstName`, `lastName`, `mobileNumber`, `email`, `creationDate`, `statusId`) VALUES
('046e34ff-c15b-437c-9d0e-3781d975a6a8', 'Gril', 'Joe', '0781587081', 'Kkk@tt.com', '2021-09-06 23:59:15', 1),
('0d346480-820e-47c2-ac0c-e865b8bb49ab', 'Trysts', 'b', '0781587081', 'Jnsnsn@gmail.com', '2021-08-31 15:41:16', 1),
('34232a46-ed41-42de-a744-35f6a5928394', 'Teetered', 'bdbdb', '0781587081', 'james2yiga@gmail.com', '2021-09-06 23:34:27', 1),
('368f317c-b45d-42bc-a88c-98b21e471dda', 'Test', 'Test', '0781587081', 'Test@code360ds.com', '2021-08-31 10:19:22', 1),
('3827c646-5834-46fa-89d0-132e42364fa8', 'Test', 'Test', '0781587081', 'Test@code360ds.com', '2021-08-31 09:25:19', 1),
('38418485-5552-4290-a196-b67beef99b03', 'Peace', 'Nakyeyune', '0781587081', 'james2yiga@gmail.com', '2021-06-26 21:36:34', 2),
('3b40b098-a817-4c92-85cb-f7b75664f154', 'Yeh', 'bdbbd', '0781587081', 'Jajaj@gmail.com', '2021-08-31 15:20:51', 1),
('5e233548-f283-45e9-8035-5b8531c55a09', 'Test', 'tyysb', '0781587081', 'Testing@cod.com', '2021-08-31 12:03:04', 1),
('603613cf-b6a4-4428-9880-dc14e6e7f784', 'Test', 'tyysb', '0781587081', 'Testing@cod.com', '2021-08-31 12:02:33', 1),
('61b14151-1978-4750-99c9-8a1fb20c2083', 'Teeth', 'ghdhd', '0781587081', 'kkkh@gmail.com', '2021-09-02 07:38:42', 1),
('693587ee-9c84-423c-a6ff-1cebe7009132', 'Yehdgsg', 'hgdgd', '0781587081', 'Jansns@gmail.com', '2021-08-31 15:22:09', 1),
('78f61d56-3c44-416a-8dfb-60db8803a1d6', 'Gggg', 'bbb', '0781587081', 'Jj@gmail.com', '2021-08-31 15:19:27', 1),
('91ad2716-30a8-4056-b936-a270ee2b95a1', 'Tesysy', 'yiga', '0781587081', 'Testing@info.com', '2021-08-31 12:00:19', 1),
('adff179d-66a0-420e-886f-4b33e915f03f', 'Send', 'db', '0781587081', 'Ken@gmail.com', '2021-09-11 07:47:08', 1),
('aeb4d933-a14d-4fe8-8e52-31fa93d6d9cf', 'Test', 'ghh', '0782587081', 'Ops@code360ds.com', '2021-08-31 15:16:56', 1),
('bf08ccaa-7363-4a8a-bc8a-d2a1f5d8e617', 'Gaffs', 'hdbdb', '0781587081', 'hhh@xonib.com', '2021-09-06 23:51:44', 1),
('bf611045-b4ec-4187-a07a-17c0add67ef4', 'Gsgsgs', 'hdbshd', '0781587081', 'Jsjsjsj@xonib.com', '2021-09-06 23:42:11', 1),
('da0cf05d-27ad-4b9a-9121-54e735c76609', 'Tetegsgd', 'bbdbd', '0781587081', 'Hshsgs@hotdick.com', '2021-09-02 07:34:46', 1),
('{67EBA0E9-6F38-0F8E-644B-B07B6B5C73D9}', 'test', 'test3', '0781587081', 'james2yiga@gmail.com', '2021-05-04 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `useroftype`
--

CREATE TABLE `useroftype` (
  `userId` varchar(50) NOT NULL,
  `userTypeId` int(11) NOT NULL,
  `isActive` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `useroftype`
--

INSERT INTO `useroftype` (`userId`, `userTypeId`, `isActive`) VALUES
('046e34ff-c15b-437c-9d0e-3781d975a6a8', 1, 1),
('0d346480-820e-47c2-ac0c-e865b8bb49ab', 1, 1),
('34232a46-ed41-42de-a744-35f6a5928394', 1, 1),
('368f317c-b45d-42bc-a88c-98b21e471dda', 1, 1),
('3827c646-5834-46fa-89d0-132e42364fa8', 1, 1),
('38418485-5552-4290-a196-b67beef99b03', 1, 1),
('3b40b098-a817-4c92-85cb-f7b75664f154', 1, 1),
('5e233548-f283-45e9-8035-5b8531c55a09', 1, 1),
('603613cf-b6a4-4428-9880-dc14e6e7f784', 1, 1),
('61b14151-1978-4750-99c9-8a1fb20c2083', 1, 1),
('693587ee-9c84-423c-a6ff-1cebe7009132', 1, 1),
('78f61d56-3c44-416a-8dfb-60db8803a1d6', 1, 1),
('91ad2716-30a8-4056-b936-a270ee2b95a1', 1, 1),
('adff179d-66a0-420e-886f-4b33e915f03f', 1, 1),
('aeb4d933-a14d-4fe8-8e52-31fa93d6d9cf', 1, 1),
('bf08ccaa-7363-4a8a-bc8a-d2a1f5d8e617', 4, 1),
('bf611045-b4ec-4187-a07a-17c0add67ef4', 1, 1),
('da0cf05d-27ad-4b9a-9121-54e735c76609', 1, 1),
('{67EBA0E9-6F38-0F8E-644B-B07B6B5C73D9}', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `userpge`
--

CREATE TABLE `userpge` (
  `id` varchar(50) NOT NULL,
  `folder` varchar(50) NOT NULL,
  `image` int(50) NOT NULL,
  `isactive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userscore`
--

CREATE TABLE `userscore` (
  `id` int(15) NOT NULL,
  `qnsanswerId` int(15) NOT NULL,
  `userId` int(15) NOT NULL,
  `democaseId` int(15) NOT NULL,
  `creationDate` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `usertype`
--

CREATE TABLE `usertype` (
  `id` int(11) NOT NULL,
  `userTypeName` varchar(50) NOT NULL,
  `isActive` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usertype`
--

INSERT INTO `usertype` (`id`, `userTypeName`, `isActive`) VALUES
(1, 'Lawyer', 1),
(3, 'Admin', 1),
(4, 'Judge', 1),
(5, 'Others', 1);

-- --------------------------------------------------------

--
-- Table structure for table `usertyperole`
--

CREATE TABLE `usertyperole` (
  `id` int(11) NOT NULL,
  `userTypeId` int(11) NOT NULL,
  `subActivityId` int(11) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT 1,
  `creationDate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `verifycode`
--

CREATE TABLE `verifycode` (
  `id` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `code` varchar(150) NOT NULL,
  `isActive` tinyint(1) DEFAULT 0,
  `creationDate` datetime DEFAULT current_timestamp(),
  `verificationDate` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verifycode`
--

INSERT INTO `verifycode` (`id`, `userId`, `code`, `isActive`, `creationDate`, `verificationDate`) VALUES
(1, '{81C65E28-F021-6319-CBE6-EDA4090FB9EA}', 'V8aB0', 1, '2021-10-13 03:46:02', '2021-10-13 07:36:05');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vwairwaybill`
-- (See below for the actual view)
--
CREATE TABLE `vwairwaybill` (
`id` varchar(150)
,`origin` varchar(250)
,`captureDate` date
,`destination` varchar(250)
,`contents` varchar(250)
,`customsValue` varchar(250)
,`insuranceValue` varchar(250)
,`dimension` varchar(150)
,`noOfPieces` int(15)
,`volWeight` float
,`actualWeight` float
,`airwaybillId` varchar(150)
,`accountNumber` varchar(150)
,`referenceName` varchar(150)
,`name` varchar(150)
,`phoneNumber` varchar(150)
,`companyName` varchar(150)
,`streetAddress` varchar(150)
,`city` varchar(150)
,`postalCode` varchar(150)
,`country` varchar(150)
,`airwaybillIdR` varchar(150)
,`accountNumberR` varchar(150)
,`referenceNameR` varchar(150)
,`nameR` varchar(150)
,`phoneNumberR` varchar(150)
,`companyNameR` varchar(150)
,`streetAddresR` varchar(150)
,`cityR` varchar(150)
,`postalCodeR` varchar(150)
,`countryR` varchar(150)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vwfactor`
-- (See below for the actual view)
--
CREATE TABLE `vwfactor` (
`demoCaseId` int(15)
,`productofsum` decimal(50,0)
,`itemNo` bigint(21)
,`factor` decimal(51,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vwinquirylist`
-- (See below for the actual view)
--
CREATE TABLE `vwinquirylist` (
`id` int(15)
,`demoCaseId` int(15)
,`subject` varchar(250)
,`msg` text
,`creationDate` datetime
,`inquireDate` datetime
,`status` varchar(50)
,`Name` varchar(101)
,`mobileNumber` varchar(20)
,`email` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vwjudge`
-- (See below for the actual view)
--
CREATE TABLE `vwjudge` (
`id` int(15)
,`title` varchar(150)
,`firstName` varchar(150)
,`lastName` varchar(150)
,`midName` varchar(150)
,`positionId` int(15)
,`profilebio` text
,`positionName` varchar(150)
,`path` varchar(250)
,`divisionName` varchar(150)
,`divisionId` int(15)
,`positionOrder` int(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vwquestionanswer`
-- (See below for the actual view)
--
CREATE TABLE `vwquestionanswer` (
`id` int(15)
,`qnsId` int(15)
,`answerId` int(15)
,`nextQnsId` int(15)
,`answer` varchar(50)
,`value` int(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vwreceipient`
-- (See below for the actual view)
--
CREATE TABLE `vwreceipient` (
`airwaybillIdR` varchar(150)
,`accountNumberR` varchar(150)
,`referenceNameR` varchar(150)
,`nameR` varchar(150)
,`phoneNumberR` varchar(150)
,`companyNameR` varchar(150)
,`streetAddresR` varchar(150)
,`cityR` varchar(150)
,`postalCodeR` varchar(150)
,`countryR` varchar(150)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vwshipper`
-- (See below for the actual view)
--
CREATE TABLE `vwshipper` (
`airwaybillId` varchar(150)
,`accountNumber` varchar(150)
,`referenceName` varchar(150)
,`name` varchar(150)
,`phoneNumber` varchar(150)
,`companyName` varchar(150)
,`streetAddress` varchar(150)
,`city` varchar(150)
,`postalCode` varchar(150)
,`country` varchar(150)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vwweightfactor`
-- (See below for the actual view)
--
CREATE TABLE `vwweightfactor` (
`demoCaseId` int(15)
,`productofsum` decimal(50,0)
,`itemNo` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vwworkflowhistory`
-- (See below for the actual view)
--
CREATE TABLE `vwworkflowhistory` (
`id` int(15)
,`airwaybillId` varchar(150)
,`workflowId` int(15)
,`createdBy` varchar(150)
,`creationDate` datetime
,`completedBy` varchar(150)
,`completionDate` datetime
,`isActive` int(1)
,`processName` varchar(150)
,`processDescription` varchar(250)
);

-- --------------------------------------------------------

--
-- Table structure for table `workflow`
--

CREATE TABLE `workflow` (
  `id` int(15) NOT NULL,
  `processName` varchar(150) NOT NULL,
  `processDescription` varchar(250) NOT NULL,
  `NextProcessId` int(15) NOT NULL,
  `processTypeId` int(15) DEFAULT -1,
  `entityId` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `workflow`
--

INSERT INTO `workflow` (`id`, `processName`, `processDescription`, `NextProcessId`, `processTypeId`, `entityId`) VALUES
(1, 'Created', 'A new Airway bill was capture at Head office and Assigned to the Driver', 2, NULL, NULL),
(2, 'Driver Aknowledged', 'The Driver has call and Instructed to set off for a pickup', 3, NULL, NULL),
(3, 'Driver Picks up Shipment', 'The Driver picks up the shippment', 4, NULL, NULL),
(4, 'Shipment in Transit', 'The Shipment in Transit and the driver Location is tracked', 5, NULL, NULL),
(5, 'Delivered', 'The Shipment was delivered to the airway bill Destination Address and Received by the recipient', 6, NULL, NULL),
(6, 'Closed', 'The Airway bill is closed of by the operations manager', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `workflowhistory`
--

CREATE TABLE `workflowhistory` (
  `id` int(15) NOT NULL,
  `airwaybillId` varchar(150) DEFAULT NULL,
  `workflowId` int(15) NOT NULL,
  `createdBy` varchar(150) NOT NULL,
  `creationDate` datetime DEFAULT current_timestamp(),
  `completedBy` varchar(150) DEFAULT NULL,
  `completionDate` datetime DEFAULT NULL,
  `isActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `workflowhistory`
--

INSERT INTO `workflowhistory` (`id`, `airwaybillId`, `workflowId`, `createdBy`, `creationDate`, `completedBy`, `completionDate`, `isActive`) VALUES
(6, 'E21100001', 1, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}', '2021-10-12 06:44:32', NULL, NULL, 0),
(7, 'E21100002', 1, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}', '2021-10-12 22:06:25', NULL, NULL, 0),
(8, 'E21100001', 1, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}', '2021-10-13 01:21:20', NULL, NULL, 0),
(9, 'E21100002', 2, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}', '2021-10-13 01:23:48', NULL, NULL, 1),
(10, 'E21100003', 1, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}', '2021-10-13 07:41:11', NULL, NULL, 0),
(11, 'E21100001', 2, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}', '2021-10-13 07:41:57', NULL, NULL, 1),
(12, 'E21100003', 2, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}', '2021-10-13 07:42:16', NULL, NULL, 1),
(13, 'E21100004', 1, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}', '2021-10-13 07:49:53', NULL, NULL, 0),
(14, 'E21100004', 2, '{CB706CAB-4A0F-96C7-6C80-04B65B8049E4}', '2021-10-13 07:51:53', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure for view `vwairwaybill`
--
DROP TABLE IF EXISTS `vwairwaybill`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwairwaybill`  AS  select `ab`.`id` AS `id`,`ab`.`origin` AS `origin`,`ab`.`captureDate` AS `captureDate`,`ab`.`destination` AS `destination`,`ab`.`contents` AS `contents`,`ab`.`customsValue` AS `customsValue`,`ab`.`insuranceValue` AS `insuranceValue`,`ab`.`dimension` AS `dimension`,`ab`.`noOfPieces` AS `noOfPieces`,`ab`.`volWeight` AS `volWeight`,`ab`.`actualWeight` AS `actualWeight`,`vsh`.`airwaybillId` AS `airwaybillId`,`vsh`.`accountNumber` AS `accountNumber`,`vsh`.`referenceName` AS `referenceName`,`vsh`.`name` AS `name`,`vsh`.`phoneNumber` AS `phoneNumber`,`vsh`.`companyName` AS `companyName`,`vsh`.`streetAddress` AS `streetAddress`,`vsh`.`city` AS `city`,`vsh`.`postalCode` AS `postalCode`,`vsh`.`country` AS `country`,`vwr`.`airwaybillIdR` AS `airwaybillIdR`,`vwr`.`accountNumberR` AS `accountNumberR`,`vwr`.`referenceNameR` AS `referenceNameR`,`vwr`.`nameR` AS `nameR`,`vwr`.`phoneNumberR` AS `phoneNumberR`,`vwr`.`companyNameR` AS `companyNameR`,`vwr`.`streetAddresR` AS `streetAddresR`,`vwr`.`cityR` AS `cityR`,`vwr`.`postalCodeR` AS `postalCodeR`,`vwr`.`countryR` AS `countryR` from ((`airwaybill` `ab` join `vwshipper` `vsh` on(`ab`.`id` = `vsh`.`airwaybillId`)) join `vwreceipient` `vwr` on(`ab`.`id` = `vwr`.`airwaybillIdR`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vwfactor`
--
DROP TABLE IF EXISTS `vwfactor`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwfactor`  AS  select `vwweightfactor`.`demoCaseId` AS `demoCaseId`,`vwweightfactor`.`productofsum` AS `productofsum`,`vwweightfactor`.`itemNo` AS `itemNo`,round(`vwweightfactor`.`productofsum` / (`vwweightfactor`.`itemNo` * 10),0) AS `factor` from `justicehive_db`.`vwweightfactor` ;

-- --------------------------------------------------------

--
-- Structure for view `vwinquirylist`
--
DROP TABLE IF EXISTS `vwinquirylist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwinquirylist`  AS  select `dcc`.`id` AS `id`,`dcc`.`demoCaseId` AS `demoCaseId`,`dcc`.`subject` AS `subject`,`dcc`.`msg` AS `msg`,`dcc`.`creationDate` AS `creationDate`,`dc`.`creationDate` AS `inquireDate`,`dcc`.`status` AS `status`,concat(`u`.`firstName`,' ',`u`.`lastName`) AS `Name`,`u`.`mobileNumber` AS `mobileNumber`,`u`.`email` AS `email` from ((`justicehive_db`.`democasecontact` `dcc` join `justicehive_db`.`democase` `dc` on(`dcc`.`demoCaseId` = `dc`.`id`)) join `justicehive_db`.`user` `u` on(`dc`.`userId` = `u`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vwjudge`
--
DROP TABLE IF EXISTS `vwjudge`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwjudge`  AS  select `j`.`id` AS `id`,`j`.`title` AS `title`,`j`.`firstName` AS `firstName`,`j`.`lastName` AS `lastName`,`j`.`midName` AS `midName`,`j`.`positionId` AS `positionId`,`j`.`profilebio` AS `profilebio`,`jp`.`positionName` AS `positionName`,`jupg`.`path` AS `path`,`ej`.`divisionName` AS `divisionName`,`jp`.`divisionId` AS `divisionId`,`jp`.`positionOrder` AS `positionOrder` from (((`justicehive_db`.`judge` `j` join `justicehive_db`.`judgepge` `jupg` on(`j`.`id` = `jupg`.`id`)) join `justicehive_db`.`judgeposition` `jp` on(`j`.`positionId` = `jp`.`id`)) join `justicehive_db`.`eacjdivison` `ej` on(`jp`.`divisionId` = `ej`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vwquestionanswer`
--
DROP TABLE IF EXISTS `vwquestionanswer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwquestionanswer`  AS  select `qa`.`id` AS `id`,`qa`.`qnsId` AS `qnsId`,`qa`.`answerId` AS `answerId`,`qa`.`nextQnsId` AS `nextQnsId`,`cla`.`answer` AS `answer`,`cla`.`value` AS `value` from (`justicehive_db`.`questionanswer` `qa` join `justicehive_db`.`checklistanswer` `cla` on(`qa`.`answerId` = `cla`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vwreceipient`
--
DROP TABLE IF EXISTS `vwreceipient`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwreceipient`  AS  select `abd`.`airwaybillId` AS `airwaybillIdR`,`abd`.`accountNumber` AS `accountNumberR`,`abd`.`referenceName` AS `referenceNameR`,`abd`.`name` AS `nameR`,`abd`.`phoneNumber` AS `phoneNumberR`,`abd`.`companyName` AS `companyNameR`,`abd`.`streetAddress` AS `streetAddresR`,`abd`.`city` AS `cityR`,`abd`.`postalCode` AS `postalCodeR`,`abd`.`country` AS `countryR` from (`airwaybilldetail` `abd` join `airwaybilldetailtype` `abdt` on(`abd`.`airwaybillDetailTypeId` = `abdt`.`id`)) where `abdt`.`name` = 'Recipient' ;

-- --------------------------------------------------------

--
-- Structure for view `vwshipper`
--
DROP TABLE IF EXISTS `vwshipper`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwshipper`  AS  select `abd`.`airwaybillId` AS `airwaybillId`,`abd`.`accountNumber` AS `accountNumber`,`abd`.`referenceName` AS `referenceName`,`abd`.`name` AS `name`,`abd`.`phoneNumber` AS `phoneNumber`,`abd`.`companyName` AS `companyName`,`abd`.`streetAddress` AS `streetAddress`,`abd`.`city` AS `city`,`abd`.`postalCode` AS `postalCode`,`abd`.`country` AS `country` from (`airwaybilldetail` `abd` join `airwaybilldetailtype` `abdt` on(`abd`.`airwaybillDetailTypeId` = `abdt`.`id`)) where `abdt`.`name` = 'Shipper' ;

-- --------------------------------------------------------

--
-- Structure for view `vwweightfactor`
--
DROP TABLE IF EXISTS `vwweightfactor`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwweightfactor`  AS  select `dc`.`demoCaseId` AS `demoCaseId`,sum(`ans`.`value` * `p`.`weight`) AS `productofsum`,count(0) AS `itemNo` from ((((`justicehive_db`.`democaseanswer` `dc` join `justicehive_db`.`questionanswer` `qa` on(`dc`.`ansId` = `qa`.`id`)) join `justicehive_db`.`checklistanswer` `ans` on(`qa`.`answerId` = `ans`.`id`)) join `justicehive_db`.`checklist` `qns` on(`qa`.`qnsId` = `qns`.`id`)) join `justicehive_db`.`rankingval` `p` on(`qns`.`priorty` = `p`.`id`)) group by `dc`.`demoCaseId` ;

-- --------------------------------------------------------

--
-- Structure for view `vwworkflowhistory`
--
DROP TABLE IF EXISTS `vwworkflowhistory`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vwworkflowhistory`  AS  select `wfh`.`id` AS `id`,`wfh`.`airwaybillId` AS `airwaybillId`,`wfh`.`workflowId` AS `workflowId`,`wfh`.`createdBy` AS `createdBy`,`wfh`.`creationDate` AS `creationDate`,`wfh`.`completedBy` AS `completedBy`,`wfh`.`completionDate` AS `completionDate`,`wfh`.`isActive` AS `isActive`,`wf`.`processName` AS `processName`,`wf`.`processDescription` AS `processDescription` from (`workflowhistory` `wfh` join `workflow` `wf` on(`wfh`.`workflowId` = `wf`.`id`)) where `wfh`.`isActive` = 1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airwaybill`
--
ALTER TABLE `airwaybill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airwaybillcharge`
--
ALTER TABLE `airwaybillcharge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airwaybillchargetemplate`
--
ALTER TABLE `airwaybillchargetemplate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airwaybilldetail`
--
ALTER TABLE `airwaybilldetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airwaybilldetailtype`
--
ALTER TABLE `airwaybilldetailtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airwaybilldriver`
--
ALTER TABLE `airwaybilldriver`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airwaybillinstruction`
--
ALTER TABLE `airwaybillinstruction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apiroute`
--
ALTER TABLE `apiroute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applyrequirement`
--
ALTER TABLE `applyrequirement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `casecontent`
--
ALTER TABLE `casecontent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `casehearing`
--
ALTER TABLE `casehearing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `casejudge`
--
ALTER TABLE `casejudge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `casenumber`
--
ALTER TABLE `casenumber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `casepge`
--
ALTER TABLE `casepge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `caseruling`
--
ALTER TABLE `caseruling`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checklist`
--
ALTER TABLE `checklist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checklistanswer`
--
ALTER TABLE `checklistanswer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collectionsite`
--
ALTER TABLE `collectionsite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configurationsetting`
--
ALTER TABLE `configurationsetting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consigeeaccount`
--
ALTER TABLE `consigeeaccount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courtclerk`
--
ALTER TABLE `courtclerk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `democase`
--
ALTER TABLE `democase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `democaseanswer`
--
ALTER TABLE `democaseanswer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `democasecontact`
--
ALTER TABLE `democasecontact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documentaccessmode`
--
ALTER TABLE `documentaccessmode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eacjdivison`
--
ALTER TABLE `eacjdivison`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entity`
--
ALTER TABLE `entity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entityfield`
--
ALTER TABLE `entityfield`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entityform`
--
ALTER TABLE `entityform`
  ADD PRIMARY KEY (`colName`);

--
-- Indexes for table `errorhandler`
--
ALTER TABLE `errorhandler`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guitoolbox`
--
ALTER TABLE `guitoolbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `judge`
--
ALTER TABLE `judge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `judgepge`
--
ALTER TABLE `judgepge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `judgeposition`
--
ALTER TABLE `judgeposition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listhandle`
--
ALTER TABLE `listhandle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mauthentication`
--
ALTER TABLE `mauthentication`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobileuser`
--
ALTER TABLE `mobileuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panel`
--
ALTER TABLE `panel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proc`
--
ALTER TABLE `proc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procdoc`
--
ALTER TABLE `procdoc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `process`
--
ALTER TABLE `process`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questionanswer`
--
ALTER TABLE `questionanswer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rankingval`
--
ALTER TABLE `rankingval`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registry`
--
ALTER TABLE `registry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rolemanagement`
--
ALTER TABLE `rolemanagement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roleId` (`roleId`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `searchword`
--
ALTER TABLE `searchword`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipperdetail`
--
ALTER TABLE `shipperdetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signup`
--
ALTER TABLE `signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stage`
--
ALTER TABLE `stage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statu`
--
ALTER TABLE `statu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subactivity`
--
ALTER TABLE `subactivity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `systemuser`
--
ALTER TABLE `systemuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toolbox`
--
ALTER TABLE `toolbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `statusId` (`statusId`),
  ADD KEY `mobileNumber` (`mobileNumber`);

--
-- Indexes for table `useroftype`
--
ALTER TABLE `useroftype`
  ADD PRIMARY KEY (`userId`,`userTypeId`),
  ADD KEY `userTypeId` (`userTypeId`);

--
-- Indexes for table `userpge`
--
ALTER TABLE `userpge`
  ADD KEY `id` (`id`);

--
-- Indexes for table `userscore`
--
ALTER TABLE `userscore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usertype`
--
ALTER TABLE `usertype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usertyperole`
--
ALTER TABLE `usertyperole`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userTypeId` (`userTypeId`),
  ADD KEY `subActivityId` (`subActivityId`);

--
-- Indexes for table `verifycode`
--
ALTER TABLE `verifycode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `workflow`
--
ALTER TABLE `workflow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `workflowhistory`
--
ALTER TABLE `workflowhistory`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `airwaybillcharge`
--
ALTER TABLE `airwaybillcharge`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `airwaybillchargetemplate`
--
ALTER TABLE `airwaybillchargetemplate`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `airwaybilldetail`
--
ALTER TABLE `airwaybilldetail`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `airwaybilldetailtype`
--
ALTER TABLE `airwaybilldetailtype`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `airwaybilldriver`
--
ALTER TABLE `airwaybilldriver`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `airwaybillinstruction`
--
ALTER TABLE `airwaybillinstruction`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `apiroute`
--
ALTER TABLE `apiroute`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `applyrequirement`
--
ALTER TABLE `applyrequirement`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `casecontent`
--
ALTER TABLE `casecontent`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `casehearing`
--
ALTER TABLE `casehearing`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `casejudge`
--
ALTER TABLE `casejudge`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `casenumber`
--
ALTER TABLE `casenumber`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `casepge`
--
ALTER TABLE `casepge`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `caseruling`
--
ALTER TABLE `caseruling`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `checklistanswer`
--
ALTER TABLE `checklistanswer`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `collectionsite`
--
ALTER TABLE `collectionsite`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `configurationsetting`
--
ALTER TABLE `configurationsetting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `courtclerk`
--
ALTER TABLE `courtclerk`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `democase`
--
ALTER TABLE `democase`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `democaseanswer`
--
ALTER TABLE `democaseanswer`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `democasecontact`
--
ALTER TABLE `democasecontact`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `documentaccessmode`
--
ALTER TABLE `documentaccessmode`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `eacjdivison`
--
ALTER TABLE `eacjdivison`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `entity`
--
ALTER TABLE `entity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `entityfield`
--
ALTER TABLE `entityfield`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=343;

--
-- AUTO_INCREMENT for table `errorhandler`
--
ALTER TABLE `errorhandler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `judge`
--
ALTER TABLE `judge`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `judgeposition`
--
ALTER TABLE `judgeposition`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `listhandle`
--
ALTER TABLE `listhandle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `mauthentication`
--
ALTER TABLE `mauthentication`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `panel`
--
ALTER TABLE `panel`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procdoc`
--
ALTER TABLE `procdoc`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `process`
--
ALTER TABLE `process`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questionanswer`
--
ALTER TABLE `questionanswer`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `rolemanagement`
--
ALTER TABLE `rolemanagement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=426;

--
-- AUTO_INCREMENT for table `searchword`
--
ALTER TABLE `searchword`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `shipperdetail`
--
ALTER TABLE `shipperdetail`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `signup`
--
ALTER TABLE `signup`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stage`
--
ALTER TABLE `stage`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `statu`
--
ALTER TABLE `statu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subactivity`
--
ALTER TABLE `subactivity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;

--
-- AUTO_INCREMENT for table `toolbox`
--
ALTER TABLE `toolbox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `userscore`
--
ALTER TABLE `userscore`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usertype`
--
ALTER TABLE `usertype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `usertyperole`
--
ALTER TABLE `usertyperole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `verifycode`
--
ALTER TABLE `verifycode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `workflow`
--
ALTER TABLE `workflow`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `workflowhistory`
--
ALTER TABLE `workflowhistory`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rolemanagement`
--
ALTER TABLE `rolemanagement`
  ADD CONSTRAINT `rolemanagement_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `subactivity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rolemanagement_ibfk_2` FOREIGN KEY (`roleId`) REFERENCES `subactivity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rolemanagement_ibfk_4` FOREIGN KEY (`userId`) REFERENCES `systemuser` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `useroftype`
--
ALTER TABLE `useroftype`
  ADD CONSTRAINT `useroftype_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `useroftype_ibfk_2` FOREIGN KEY (`userTypeId`) REFERENCES `usertype` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `usertyperole`
--
ALTER TABLE `usertyperole`
  ADD CONSTRAINT `usertyperole_ibfk_1` FOREIGN KEY (`userTypeId`) REFERENCES `usertype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usertyperole_ibfk_2` FOREIGN KEY (`subActivityId`) REFERENCES `subactivity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
